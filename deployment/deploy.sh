#!/bin/bash

#Refresh frontend
git pull
cd frontend
#sudo export PROD=True
rm .env.local
echo "REACT_APP_API_LINK=\"https://cs373-pokedex.com\"" > .env
npm install
npm run-script build
sudo systemctl restart nginx
sudo systemctl restart flask-api.service