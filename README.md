# Pokemon Index
## cs373-idb project by group 1

A version of this project is hosted at https://cs373-pokedex.com and https://cs373.com

### Installing dependencies
1. run `make dependencies`

### How to run backend
python version `3.6.8`
1. Install dependencies
2. Run `make backend`

### How to run frontend
node version `v10.19.0`
1. Install depdendencies
2. Run `make frontend`

### How to run tests
1. Install dependencies
2. Run `make tests`


#### Citations
- Some boilerplate code taken from provided github repo: https://github.com/emaanhaseem/cs373-idb/
