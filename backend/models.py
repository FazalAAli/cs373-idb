"""
To generate HTML documentation for this module issue the
command:

python -m pydoc -w models

"""


from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.inspection import inspect
from sqlalchemy.orm import query
db = SQLAlchemy()

"""
DATABASE SCHEMA
PILLARS:
Types: (id, name, generation, double_damage_to_name, double_damage_to_id, no_damage_to_name,
        no_damaage_to_id, pokemon_count,pokemon_list,pokemon_list_id, moves_count, move_list, move_list_id, dub_damage_count, no_damage_count)

Pokemon:(id, name, sprite, moves_count, move_list, move_list_id, type,type_id, ability_count, base_stat_total)
weight, height, base_xp, card_image)

Moves: (id, name,accuracy, pp, generation, power,
        type, type_id, pokemon_list, pokemon_list_id, image)
"""


class Type(db.Model):
    """
    Defines the Type attribute model in the pokemon world. 
    (id, name, generation, double_damage_to_name, double_damage_to_id, no_damage_to_name,
        no_damaage_to_id, pokemon_count,pokemon_list,pokemon_list_id, moves_count, move_list, move_list_id, dub_damage_count, no_damage_count)

    """
    __searchable__ = ["name", "generation", "moves_count",
                      "pokemon_count", "dub_damage_count", "no_damage_count"]
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(), unique=True, nullable=False)
    generation = db.Column(db.String(), nullable=False)
    double_damage_list_id_csv = db.Column(db.String())
    no_damage_list_id_csv = db.Column(db.String())
    pokemon_count = db.Column(db.Integer, nullable=False, default=0)
    pokemon_list_id_csv = db.Column(db.String())
    moves_count = db.Column(db.Integer, nullable=False, default=0)
    move_list_id_csv = db.Column(db.String())
    dub_damage_count = db.Column(db.Integer, nullable=False, default=0)
    no_damage_count = db.Column(db.Integer, nullable=False, default=0)

    def get_json(self):
        return master_get_json(self)


class Move(db.Model):
    """
    Defines the move model in the pokemon world.
    (id, name,accuracy, pp, generation, power,
        type, type_id, pokemon_list, pokemon_list_id, image)

    """
    __searchable__ = ["name", "generation", "accuracy", "pp", "power", "type"]
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(), unique=True, nullable=False)
    generation = db.Column(db.String(), nullable=False)
    accuracy = db.Column(db.Integer, nullable=False, default=0)
    pp = db.Column(db.Integer, nullable=False, default=0)
    power = db.Column(db.Integer, nullable=False, default=0)
    type = db.Column(db.String())
    type_id = db.Column(db.Integer)
    pokemon_list_id_csv = db.Column(db.String())
    image = db.Column(db.String())

    def get_json(self):
        return master_get_json(self)


class Pokemon(db.Model):
    """
    Defines the Pokemon model in the pokemon world
    (id, name, sprite, moves_count, move_list, move_list_id, type,type_id, ability_count, base_stat_total)
    """
    __searchable__ = ["name", "type", "moves_count",
                      "weight", "height", "base_xp", "base_stat_total"]

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    sprite = db.Column(db.String())
    moves_count = db.Column(db.Integer, nullable=False, default=0)
    move_list_id_csv = db.Column(db.String())
    type = db.Column(db.String())
    type_id = db.Column(db.Integer)
    ability_count = db.Column(db.Integer, nullable=False, default=0)
    card_image = db.Column(db.String())
    base_stat_total = db.Column(db.Integer, nullable=False, default=0)
    weight = db.Column(db.Integer, nullable=False, default=0)
    height = db.Column(db.Integer, nullable=False, default=0)
    base_xp = db.Column(db.Integer, nullable=False, default=0)

    def get_json(self):
        return master_get_json(self)


def master_get_json(callerInstance):
    """
    Converts all attributes to a dictionary so they can be sent as a json
    """
    keys = [column.name for column in inspect(type(callerInstance)).c]
    exclusion_list = ["move_list_id_csv",
                      "pokemon_list_id_csv", "double_damage_list_id_csv", "no_damage_list_id_csv"]
    response_dict = {}
    for key in keys:
        if key not in exclusion_list:
            value = getattr(callerInstance, key)
            response_dict[key] = value
        else:
            temp_key = key[:-4]  # pokemon_list_id or move_list_id at this point # nopep8
            data_needed = getattr(callerInstance, key)
            if data_needed:
                response_dict[temp_key] = data_needed.split(",")
                temp_key_list = temp_key[:-3]  # pokemon_list or move_list
                name_list = []
                for id in response_dict[temp_key]:
                    # print(f"Getting {id}")
                    model_string = temp_key_list[:-5]
                    if model_string in model_map.keys():
                        model_name = model_map[temp_key_list[:-5]]
                    else:
                        model_name = Type
                    try:
                        name_list.append(db.session.query(
                            model_name).get(id).name)
                    except:
                        pass
                response_dict[temp_key_list] = name_list

    return response_dict


model_map = {
    "pokemon": Pokemon,
    "move": Move,
    "type": Type,
}
