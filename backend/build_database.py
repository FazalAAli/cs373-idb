from main import app, db
from models import *
import requests
import json
DEBUG = False


def debug_print(str):
    if DEBUG:
        print(str)


def gen_map_conversion(roman_generation_string):
    roman_generation = roman_generation_string.split("-")[1]
    gen_map = ["i", "ii", "iii", "iv", "v", "vi", "vii", "viii", "ix", "x"]
    possible_index = gen_map.index(roman_generation) + 1
    return possible_index


def scraper():
    debug_print("Starting scrape")
    for i in range(1, 19):
        type_data = json.loads(requests.get(
            f"http://localhost:5000/api/api/get_specific_type/{i}").text)["response"][0]

        if not db.session.query(Type).get(int(type_data["id"])):
            debug_print(f"Getting type {type_data['id']}")
            put_in_type(type_data)
            scrape_moves(type_data["move_list_id"], True)
            scrape_pokemon(type_data["pokemon_list_id"], True)


def put_in_type(type_data):
    type_mapping = {
        "id": int(type_data["id"]),
        "name": type_data["name"],
        "generation": gen_map_conversion(type_data["generation"]),
        "pokemon_count": type_data["pokemon_count"],
        "pokemon_list_id_csv": ",".join(type_data["pokemon_list_id"]),
        "moves_count": type_data["moves_count"],
        "move_list_id_csv": ",".join(type_data["move_list_id"])
    }
    if "double_damage_to_name" in type_data:
        type_mapping["double_damage_list_id_csv"] = ",".join(
            type_data["double_damage_to_id"])

        type_mapping["dub_damage_count"] = len(
            type_data["double_damage_to_id"])

    if "no_damage_to_name" in type_data:
        type_mapping["no_damage_list_id_csv"] = ",".join(
            type_data["no_damage_to_id"])

        type_mapping["no_damage_count"] = len(
            type_data["no_damage_to_id"])

    current = Type(**type_mapping)
    db.session.add(current)
    db.session.commit()


def scrape_moves(move_list_id, scrapeChildren):
    for entry in move_list_id:
        if not db.session.query(Move).get(int(entry)):
            debug_print(f"Getting move {entry}")
            move_data = json.loads(requests.get(
                f"http://localhost:5000/api/api/get_specific_move/{entry}").text)["response"][0]
            put_in_move(move_data)
            if scrapeChildren:
                scrape_pokemon(move_data["pokemon_list_id"], False)
        else:
            debug_print(f"Already have move {entry}")


def put_in_move(move_data):
    move_mapping = {
        "id": int(move_data["id"]),
        "name": move_data["name"],
        "generation": gen_map_conversion(move_data["generation"]),
        "accuracy": move_data["accuracy"],
        "pp": move_data["pp"],
        "power": move_data["power"],
        "type": move_data["type"],
        "type_id": move_data["type_id"],
        "pokemon_list_id_csv": ",".join(move_data["pokemon_list_id"]),
    }

    if "image" in move_data:
        move_mapping["image"] = move_data["image"]

    current = Move(**move_mapping)
    db.session.add(current)
    db.session.commit()


def scrape_pokemon(pokemon_list_id, scrapeChildren):
    for entry in pokemon_list_id:
        if not db.session.query(Pokemon).get(int(entry)):
            debug_print(f"Getting pokemon {entry}")
            pokemon_data = json.loads(requests.get(
                f"http://localhost:5000/api/api/get_specific_pokemon/{entry}").text)["response"][0]

            put_in_pokemon(pokemon_data)

            if scrapeChildren:
                scrape_moves(pokemon_data["move_list_id"], False)
        else:
            debug_print(f"Already have pokemon {entry}")


def put_in_pokemon(pokemon_data):
    pokemon_mapping = {
        "id": int(pokemon_data["id"]),
        "name": pokemon_data["name"],
        "moves_count": pokemon_data["moves_count"],
        "move_list_id_csv": ",".join(pokemon_data["move_list_id"]),
        "type":  pokemon_data["type"],
        "type_id": pokemon_data["type_id"],
        "ability_count": pokemon_data["ability_count"],
        "base_stat_total": pokemon_data["base_stat_total"],
        "weight": int(pokemon_data["weight"])/10,
        "height": int(pokemon_data["height"])/10,
        "base_xp": pokemon_data["base_xp"]
    }

    if "sprite" in pokemon_data:
        pokemon_mapping["sprite"] = pokemon_data["sprite"]

    if "card_image" in pokemon_data:
        pokemon_mapping["card_image"] = pokemon_data["card_image"]

    current = Pokemon(**pokemon_mapping)
    db.session.add(current)
    db.session.commit()


if __name__ == "__main__":
    with app.app_context():
        answer = input("Force build? y/n \n")
        if(answer == "y"):
            db.drop_all()
        db.create_all()
        scraper()
        print("Done")
