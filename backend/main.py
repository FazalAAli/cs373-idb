from operator import indexOf, mod
from flask import jsonify, Flask, request
from flask_cors import CORS
from flask_caching import Cache
from subprocess import check_output
from flask_sqlalchemy import model
from sqlalchemy.sql.sqltypes import NULLTYPE
from sqlalchemy import or_
from models import *
from utility import *
import config
import re as regex
import os

app = Flask(__name__, instance_relative_config=True)
CORS(app)
app.config.from_mapping(config.config_dict)
db.init_app(app)
cache = Cache(app)

"""
DISPLAY SCHEMA
Tables page:
TYPES: Name, Generation, Num dub damage, Num No damage, Num pokemon, Num Moves
POKEMON: Name, Num Moves, Type, Ability count, base_stat, weight, height, base_xp
Moves: Name, generation, pp, power, accuracy, type, num pokemon

Instance page:
Types: Name, generation dub damage names, no damage names, pokemons, moves,
Pokemon: name, sprite, card, moves, weight, height, base xp, base_stat_total, ability count, type
Moves: name, type, generation, power, pp, accuracy, pokemons, image
"""

"""
DATABASE SCHEMA
PILLARS:
Types: (id, name, generation, double_damage_to_name, double_damage_to_id, no_damage_to_name,
        no_damaage_to_id, pokemon_count,pokemon_list,pokemon_list_id, moves_count, move_list, move_list_id, dub_damage_count, no_damage_count)

Pokemon:(id, name, sprite, moves_count, move_list, move_list_id, type,type_id, ability_count, base_stat_total
weight, height, base_xp, card_image)

Moves: (id, name,accuracy, pp, generation, power,
        type, type_id, pokemon_list, pokemon_list_id, image)
"""


@app.route("/api/")
def index():
    return "API is reachable"


@app.route("/api/get_num_tests/")
@cache.cached(timeout=120)
def count_tests():
    test_dir = "backend/tests/"
    all_files = ["backend_tests.py", "db_tests.py",
                 "../../frontend/tests/tests.py"]
    all_found = {'fazal': 0, 'carter': 0, 'rice': 0, 'lawson': 9, 'trevor': 0}
    for file_name in all_files:
        file_name = test_dir + file_name
        file = open(os.path.join(
            config.config_dict["ROOT_PATH"], file_name), 'r')
        for line in file:
            match = regex.findall("@@.+", line)
            if len(match) > 0:
                all_found[match[0][2:].lower()] += 1
    return jsonify(all_found)


@ app.route("/api/get_specific_<category>/<int:id>")
@cache.memoize()
def db_get_specific(category, id):
    response = []

    obj_from_db = db.session.query(
        model_map[category]).get(int(id))

    if(obj_from_db):
        response.append(obj_from_db.get_json())

    return(jsonify({
        "size": len(response),
        "response": response
    }))


@ app.route("/api/get_<category>_page/")
def db_get_page(category):
    args = get_args_from_request(request.args)
    sorter = get_sorter_from_args(args["desc"], args["sort"])
    model_name = model_map[category]

    keyword = str(args["search"])
    if(len(keyword) > 0):
        if " " in keyword or "&&" in keyword or "and" in keyword:
            page = db_multi_search(keyword, category)
        else:
            page = db_search(keyword, category)
    else:
        page = model_name.query.filter()

    for key in args.keys():
        if "filter" in key:
            filter = key[:-7]
            if(len(args[key]) > 1):
                page = page.filter(getattr(model_name, filter) >= args[key][0])
                page = page.filter(getattr(model_name, filter) <= args[key][1])
            else:
                if len(args[key][0]) > 0:
                    page = page.filter(
                        getattr(model_name, filter) == args[key][0])

    page = page.order_by(sorter)

    page = page.paginate(args["page_num"], args["num_per_page"])
    db_data = page.items
    response = [x.get_json() for x in db_data]
    pages = [x for x in page.iter_pages()]

    return jsonify({
        "pages": pages,
        "size": len(response),
        "response": response
    })


def db_search(keyword, category):
    model_name = model_map[category]
    search_params = get_search_params(keyword, model_name)
    page = model_name.query.filter(or_(*search_params))
    return page


def get_search_params(keyword, model_name):
    keyword = keyword.lower()
    searchable_attrs = model_name.__searchable__
    search_params = []
    for attribute in searchable_attrs:
        attr = getattr(model_name, attribute)
        search_params.append(attr.contains(keyword))

    return search_params


def db_multi_search(query, category):
    model_name = model_map[category]
    and_delims = ["&&", "and", "AND"]
    or_delims = ["||", " ", "or", "OR"]
    and_checks = [delim in query for delim in and_delims]
    and_search = any(and_checks)
    keywords = []

    if and_search:
        delim_index = indexOf(and_checks, True)
        keywords = [x.strip() for x in query.split(and_delims[delim_index])]
    else:
        or_checks = [delim in query for delim in or_delims]
        delim_index = indexOf(or_checks, True)
        keywords = [x.strip() for x in query.split(or_delims[delim_index])]

    all_queries = []
    for keyword in keywords:
        search_params = get_search_params(keyword, model_name)
        all_queries.append(or_(*search_params))

    if and_search:
        page = model_name.query.filter(*all_queries)
    else:
        page = model_name.query.filter(or_(*all_queries))

    return page


@ app.route("/api/get_<category>_range/<int:start>/<int:end>")
@ cache.memoize()
def db_get_range(category, start, end):
    if end < start:
        start, end = end, start

    if start < 0:
        return "Can't have id less than 0"

    response = []
    for i in range(int(start), int(end)+1):
        obj_from_db = db.session.query(model_map[category]).get(i)
        if(obj_from_db):
            response.append(obj_from_db.get_json())

    return jsonify({
        "size": len(response),
        "response": response
    })


@ app.route("/api/run_tests/")
@ cache.cached()
def run_tests():
    current_path = os.getcwd()
    os.chdir(config.config_dict["ROOT_PATH"])
    try:
        output = check_output(["make", "tests"])
    except:
        print("Error running tests - please run tests manually")
        return jsonify({"response": "Error running tests - please try again later"})
    os.chdir(current_path)
    return jsonify({
        "response": output.decode("UTF-8")
    })


from api_endpoints import *  # nopep8
if __name__ == "__main__":
    app.run(host="localhost", port=5000)
