import os
from sqlalchemy import desc


def get_args_from_request(web_args):
    keys = [x for x in web_args]
    values = []
    for key in keys:
        if("filter" in key):
            value = process_filter(key, web_args)
        else:
            try:
                value = int(web_args[key])
            except:
                value = web_args[key]
        values.append(value)
    args = dict(zip(keys, values))
    return args


def process_filter(key, web_args):
    value_as_strings = web_args[key].split(",")
    value = []
    for possible_num in value_as_strings:
        try:
            value.append(int(possible_num))
        except:
            value.append(possible_num)

    return value


def get_sorter_from_args(desc_pref, sort):
    if desc_pref == "desc":
        return desc(sort)
    else:
        return sort
