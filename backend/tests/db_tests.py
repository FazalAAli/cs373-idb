from unittest import main, TestCase
import json
from test_client import database, app
from test_client import Pokemon, Type, Move


class DatabaseTests(TestCase):
    # @@fazal
    def test_specific_pokemon(self):
        response = database.session.query(
            Pokemon).get(1)

        d = response
        self.assertEqual(d.id, 1)
        self.assertEqual(d.name, "bulbasaur")
        self.assertEqual(d.type, "grass")

    # @@fazal
    def test_specific_type(self):
        response = database.session.query(
            Type).get(1)

        d = response
        self.assertEqual(d.id, 1)
        self.assertEqual(d.name, "normal")
        self.assertEqual(d.moves_count, 188)
        self.assertEqual(d.pokemon_count, 130)

    # @@fazal
    def test_specific_move(self):
        response = database.session.query(
            Move).get(1)

        d = response
        self.assertEqual(d.id, 1)
        self.assertEqual(d.name, "pound")
        self.assertEqual(d.accuracy, 100)

    # @@fazal
    def test_type_get_all(self):
        response = database.session.query(
            Type).all()
        d = response
        self.assertEqual(len(d), 18)

    # @@fazal
    def test_move_get_all(self):
        response = database.session.query(
            Move).all()
        d = response
        self.assertEqual(len(d), 826)

    # @@fazal
    def test_pokemon_get_all(self):
        response = database.session.query(
            Pokemon).all()
        d = response
        self.assertEqual(len(d), 1118)

    # @@fazal
    def test_type_has_all_pokemon(self):
        type = database.session.query(
            Type).get(2)
        type_num_pokemon = type.pokemon_count

        num_type_pokemon = database.session.query(
            Pokemon).filter(Pokemon.type_id == 2).all()
        self.assertLessEqual(len(num_type_pokemon), type_num_pokemon)

    # @@fazal
    def test_type_has_all_move(self):
        type = database.session.query(
            Type).get(2)
        type_num_move = type.moves_count

        num_type_move = database.session.query(
            Move).filter(Move.type_id == 2).all()
        self.assertLessEqual(len(num_type_move), type_num_move)

    # @@fazal
    def test_move_pokemon_relationship(self):
        '''
        If move lists a pokemon, the pokemon must list move
        '''
        move = database.session.query(Move).get(1)
        all_move_pokemon_csv = move.pokemon_list_id_csv
        all_move_pokemon_list = all_move_pokemon_csv.split(",")

        for pokemon in all_move_pokemon_list:
            pokemon_moves = database.session.query(
                Pokemon).get(int(pokemon)).move_list_id_csv
            self.assertIn("1", pokemon_moves)

    # @@fazal
    def test_move_json(self):
        move = database.session.query(Move).get(1)
        json_data = move.get_json()
        self.assertEqual(move.id, int(json_data["id"]))
        self.assertEqual(move.name, json_data["name"])

    # @@fazal
    def test_pokemon_json(self):
        poke = database.session.query(Pokemon).get(1)
        json_data = poke.get_json()
        self.assertEqual(poke.id, int(json_data["id"]))
        self.assertEqual(poke.name, json_data["name"])

    # @@fazal
    def test_type_json(self):
        type = database.session.query(Type).get(1)
        json_data = type.get_json()
        self.assertEqual(type.id, int(json_data["id"]))
        self.assertEqual(type.name, json_data["name"])

    # @@fazal
    def test_pokemon_data_full(self):
        '''
        Ensures all data is filled - even if it is just zero
        '''
        pokemons = database.session.query(Pokemon).all()

        for pokemon in pokemons:
            self.assertGreaterEqual(pokemon.weight, 0)
            self.assertGreaterEqual(pokemon.height, 0)
            self.assertGreaterEqual(pokemon.base_xp, 0)

    # @@fazal
    def test_move_data_full(self):
        '''
        Ensures all data is filled - even if it is just zero
        '''
        moves = database.session.query(Move).all()

        for move in moves:
            self.assertGreaterEqual(move.accuracy, 0)
            self.assertGreaterEqual(move.pp, 0)
            self.assertGreaterEqual(move.power, 0)

    # @@fazal
    def test_type_data_full(self):
        '''
        Ensures all data is filled - even if it is just zero
        '''
        types = database.session.query(Type).all()

        for type in types:
            self.assertGreaterEqual(type.pokemon_count, 0)
            self.assertGreaterEqual(type.moves_count, 0)


if __name__ == "__main__":  # pragma: no cover
    with app.app_context():
        main()
