from unittest import main, TestCase
import json
from test_client import client

# AUTOMATED TEST COUNT: JUST ADD two at signs and your name before each test you write so it can be counted
# Be sure to use lowercase name and have it be one of the following
# fazal, carter, lawson, rice, trevor
# Please see test_1 for an example of automated couting and how to call the flask api


class backend_test(TestCase):
    # @@fazal
    def test_1(self):
        response = client.get("/api/")
        self.assertEqual(response.get_data(), b"API is reachable")
    # ------------------------
    # Pokemon - Specific Call
    # ------------------------

    # @@lawson
    # Making sure that we only get a single entry
    def test_ps_1(self):
        url = "/api/get_specific_pokemon/1"
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(d["response"]), d["size"])
        self.assertEqual(len(d["response"]), 1)
        self.assertEqual(1, d["size"])

    # @@lawson
    # Making sure that we get the correct instances of the variables
    def test_ps_2(self):
        url = "/api/get_specific_pokemon/1"
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        d = json.loads(response.get_data(as_text=True))
        entry = d["response"][0]
        self.assertIsInstance(entry["ability_count"], int)
        self.assertIsInstance(entry["base_stat_total"], int)
        self.assertIsInstance(entry["base_xp"], int)
        self.assertIsInstance(entry["card_image"], str)
        self.assertIsInstance(entry["id"], int)
        self.assertIsInstance(entry["move_list"], list)
        self.assertIsInstance(entry["move_list_id"], list)
        self.assertIsInstance(entry["moves_count"], int)
        self.assertIsInstance(entry["name"], str)
        self.assertIsInstance(entry["sprite"], str)
        self.assertIsInstance(entry["type"], str)
        self.assertIsInstance(entry["type_id"], int)

    # @@lawson
    # Making sure that the contents are correct
    def test_ps_3(self):
        url = "/api/get_specific_pokemon/1"
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        d = json.loads(response.get_data(as_text=True))
        entry = d["response"][0]

        self.assertEqual(entry["ability_count"], 2)
        self.assertEqual(entry["base_stat_total"], 318)
        self.assertEqual(entry["base_xp"], 64)
        self.assertEqual(entry["card_image"],
                         "https://images.pokemontcg.io/det1/1_hires.png")
        self.assertEqual(entry["height"], 0.7)
        self.assertEqual(entry["id"], 1)
        self.assertEqual(len(entry["move_list"]), 78)
        self.assertEqual(len(entry["move_list_id"]), 78)
        self.assertEqual(entry["moves_count"], 78)
        self.assertEqual(entry["name"], "bulbasaur")
        self.assertEqual(
            entry["sprite"], "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png")
        self.assertIn(
            entry["type"],
            [
                "normal",
                "fightying",
                "flying",
                "poison",
                "ground",
                "rock",
                "bug",
                "ghost",
                "steel",
                "fire",
                "water",
                "grass",
                "electric",
                "psychic",
                "ice",
                "dragon",
                "dark",
                "fairy"
            ],
        )
        self.assertEqual(entry["type_id"], 12)
        self.assertEqual(entry["weight"], 6.9)

    # @@carter
    # Check that incorrect values gives no data back
    def test_ps_4(self):
        url = "/api/get_specific_pokemon/5000000"
        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(d["response"]), 0)

    # ---------------------
    # Pokemon - Range Call
    # ---------------------

    # @@lawson
    # Making sure we get the correct number of responses
    def test_pr_1(self):
        url = "/api/get_pokemon_range/1/5"
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(d["response"]), d["size"])
        self.assertEqual(len(d["response"]), 5)
        self.assertEqual(5, d["size"])

    # @@lawson
    # Making sure we get the correct instances
    def test_pr_2(self):
        url = "/api/get_pokemon_range/1/5"
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        d = json.loads(response.get_data(as_text=True))
        for entry in d["response"]:
            self.assertIsInstance(entry["ability_count"], int)
            self.assertIsInstance(entry["base_stat_total"], int)
            self.assertIsInstance(entry["base_xp"], int)
            self.assertIsInstance(entry["card_image"], str)
            self.assertIsInstance(entry["id"], int)
            self.assertIsInstance(entry["move_list"], list)
            self.assertIsInstance(entry["move_list_id"], list)
            self.assertIsInstance(entry["moves_count"], int)
            self.assertIsInstance(entry["name"], str)
            self.assertIsInstance(entry["sprite"], str)
            self.assertIsInstance(entry["type"], str)
            self.assertIsInstance(entry["type_id"], int)

    # @@lawson
    # Making sure that we get the correct ids for the range
    def test_pr_3(self):
        url = "/api/get_pokemon_range/1/5"
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        count = 1
        d = json.loads(response.get_data(as_text=True))
        for entry in d["response"]:
            self.assertEqual(entry["id"], count)
            count += 1

    # @@carter
    def test_pr_4(self):
        url = "/api/get_pokemon_range/5000000/5000005"
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        d = json.loads(response.get_data(as_text=True))
        for entry in d["response"]:
            self.assertEqual(len(d["response"]), 0)

    # ------------------------
    # Pokemon - Page Call
    # ------------------------

    # @@carter
    def test_pp_1(self):
        base_url = "/api/get_pokemon_page/?"
        page_num = "page_num=%d&"                % (1)
        num_per_page = "num_per_page=%d&"        % (10)
        sort = "sort=%s&"                        % ("name")
        desc = "desc=%s&"                        % ("asc")
        search = "search=%s&"                    % ("")
        weight_filter = "weight_filter=%d,%d&"   % (0, 1000)
        height_filter = "height_filter=%d,%d&"   % (0, 1000)
        base_xp_filter = "base_xp_filter=%d,%d&" % (0, 700)
        type_filter = "type_filter=%s"           % ("")
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + weight_filter + height_filter 
               + base_xp_filter + type_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(d["response"]), d["size"])
        self.assertEqual(len(d["response"]), 10)
        self.assertEqual(10, d["size"])

    # @@lawson
    def test_pp_1_2(self):
        base_url = "/api/get_pokemon_page/?"
        page_num = "page_num=%d&"                % (1)
        num_per_page = "num_per_page=%d&"        % (15)
        sort = "sort=%s&"                        % ("name")
        desc = "desc=%s&"                        % ("asc")
        search = "search=%s&"                    % ("")
        weight_filter = "weight_filter=%d,%d&"   % (0, 1000)
        height_filter = "height_filter=%d,%d&"   % (0, 1000)
        base_xp_filter = "base_xp_filter=%d,%d&" % (0, 700)
        type_filter = "type_filter=%s"           % ("")
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + weight_filter + height_filter 
               + base_xp_filter + type_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(d["response"]), d["size"])
        self.assertEqual(len(d["response"]), 15)
        self.assertEqual(15, d["size"])

    # @@fazal
    def test_pp_2(self):
        base_url = "/api/get_pokemon_page/?"
        page_num = "page_num=%d&"                % (1)
        num_per_page = "num_per_page=%d&"        % (10)
        sort = "sort=%s&"                        % ("name")
        desc = "desc=%s&"                        % ("asc")
        search = "search=%s&"                    % ("")
        weight_filter = "weight_filter=%d,%d&"   % (0, 1000)
        height_filter = "height_filter=%d,%d&"   % (0, 1000)
        base_xp_filter = "base_xp_filter=%d,%d&" % (0, 700)
        type_filter = "type_filter=%s"           % ("")
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + weight_filter + height_filter 
               + base_xp_filter + type_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        cur_name = ""
        d = json.loads(response.get_data(as_text=True))
        for entry in d["response"]:
            if cur_name == "":
                cur_name = entry["name"]
            else:
                self.assertGreaterEqual(entry["name"], cur_name)
                cur_name = entry["name"]

    # @@carter

    def test_pp_3(self):
        base_url = "/api/get_pokemon_page/?"
        page_num = "page_num=%d&"                % (30)
        num_per_page = "num_per_page=%d&"        % (10)
        sort = "sort=%s&"                        % ("name")
        desc = "desc=%s&"                        % ("desc")
        search = "search=%s&"                    % ("")
        weight_filter = "weight_filter=%d,%d&"   % (0, 1000)
        height_filter = "height_filter=%d,%d&"   % (0, 1000)
        base_xp_filter = "base_xp_filter=%d,%d&" % (0, 700)
        type_filter = "type_filter=%s"           % ("")
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + weight_filter + height_filter 
               + base_xp_filter + type_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        cur_name = ""
        d = json.loads(response.get_data(as_text=True))
        for entry in d["response"]:
            if cur_name == "":
                cur_name = entry["name"]
            else:
                self.assertLessEqual(entry["name"], cur_name)
                cur_name = entry["name"]
    
    # @@lawson
    # Checking search capabilities
    def test_pp_4(self):
        base_url = "/api/get_pokemon_page/?"
        page_num = "page_num=%d&"                % (1)
        num_per_page = "num_per_page=%d&"        % (1000)
        sort = "sort=%s&"                        % ("name")
        desc = "desc=%s&"                        % ("asc")
        search = "search=%s&"                    % ("Hello")
        weight_filter = "weight_filter=%d,%d&"   % (0, 1000)
        height_filter = "height_filter=%d,%d&"   % (0, 1000)
        base_xp_filter = "base_xp_filter=%d,%d&" % (0, 700)
        type_filter = "type_filter=%s"           % ("")
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + weight_filter + height_filter 
               + base_xp_filter + type_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(d["response"]), d["size"])
        self.assertEqual(len(d["response"]), 1)
        self.assertEqual(1, d["size"])

    # @@lawson
    # Further testing search
    def test_pp_5(self):
        search_query = "Pika"

        base_url = "/api/get_pokemon_page/?"
        page_num = "page_num=%d&"                % (1)
        num_per_page = "num_per_page=%d&"        % (1000)
        sort = "sort=%s&"                        % ("name")
        desc = "desc=%s&"                        % ("asc")
        search = "search=%s&"                    % (search_query)
        weight_filter = "weight_filter=%d,%d&"   % (0, 1000)
        height_filter = "height_filter=%d,%d&"   % (0, 1000)
        base_xp_filter = "base_xp_filter=%d,%d&" % (0, 700)
        type_filter = "type_filter=%s"           % ("")
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + weight_filter + height_filter 
               + base_xp_filter + type_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(d["response"]), d["size"])
        self.assertEqual(len(d["response"]), 15)
        self.assertEqual(15, d["size"])

        for entry in d["response"]:
            self.assertTrue(search_query.lower() in entry["name"].lower())
    
    # Testing filtering on weight
    # @@lawson
    def test_pp_6(self):
        weight_range = (1,2)

        base_url = "/api/get_pokemon_page/?"
        page_num = "page_num=%d&"                % (1)
        num_per_page = "num_per_page=%d&"        % (1000)
        sort = "sort=%s&"                        % ("name")
        desc = "desc=%s&"                        % ("asc")
        search = "search=%s&"                    % ("")
        weight_filter = "weight_filter=%d,%d&"   % (weight_range[0], weight_range[1])
        height_filter = "height_filter=%d,%d&"   % (0, 1000)
        base_xp_filter = "base_xp_filter=%d,%d&" % (0, 700)
        type_filter = "type_filter=%s"           % ("")
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + weight_filter + height_filter 
               + base_xp_filter + type_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        
        d = json.loads(response.get_data(as_text=True))
        for entry in  d["response"]:
            self.assertLessEqual(entry["weight"], weight_range[1])
            self.assertGreaterEqual(entry["weight"], weight_range[0])
    
    # Testing a bad filtering call
    # @@lawson
    def test_pp_7(self):
        base_url = "/api/get_pokemon_page/?"
        page_num = "page_num=%d&"                % (1)
        num_per_page = "num_per_page=%d&"        % (1000)
        sort = "sort=%s&"                        % ("name")
        desc = "desc=%s&"                        % ("asc")
        search = "search=%s&"                    % ("")
        weight_filter = "weight_filter=%d,%d&"   % (0, 1000)
        height_filter = "height_filter=%d,%d&"   % (0, 1000)
        base_xp_filter = "base_xp_filter=%d,%d&" % (0, 700)
        type_filter = "type_filter=%s"           % ("green")
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + weight_filter + height_filter 
               + base_xp_filter + type_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(d["response"]), 0)

    # Testing a bad filtering call v2
    # @@lawson
    def test_pp_8(self):
        base_url = "/api/get_pokemon_page/?"
        page_num = "page_num=%d&"                % (1)
        num_per_page = "num_per_page=%d&"        % (1000)
        sort = "sort=%s&"                        % ("name")
        desc = "desc=%s&"                        % ("asc")
        search = "search=%s&"                    % ("")
        weight_filter = "weight_filter=%d,%d&"   % (0, 1000)
        height_filter = "height_filter=%d,%d&"   % (0, 1000)
        base_xp_filter = "base_xp_filter=%d,%d&" % (20, 10)
        type_filter = "type_filter=%s"           % ("")
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + weight_filter + height_filter 
               + base_xp_filter + type_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(d["response"]), 0)

    # Testing a bad filtering call v3
    # @@lawson
    def test_pp_9(self):
        base_url = "/api/get_pokemon_page/?"
        page_num = "page_num=%d&"                % (1)
        num_per_page = "num_per_page=%d&"        % (1000)
        sort = "sort=%s&"                        % ("name")
        desc = "desc=%s&"                        % ("asc")
        search = "search=%s&"                    % ("")
        weight_filter = "weight_filter=%d,%d&"   % (0, 1000)
        height_filter = "height_filter=%d,%d&"   % (0, 1000)
        base_xp_filter = "base_xp_filter=%d,%d&" % (0, 700)
        type_filter = "type_filter=%s"           % ("Invalidtype")
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + weight_filter + height_filter 
               + base_xp_filter + type_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(d["response"]), 0)

    # Testing sort and desc to see if they work correctly v1
    # @@lawson
    def test_pp_10(self):
        base_url = "/api/get_pokemon_page/?"
        page_num = "page_num=%d&"                % (1)
        num_per_page = "num_per_page=%d&"        % (1000)
        sort = "sort=%s&"                        % ("type")
        desc = "desc=%s&"                        % ("asc")
        search = "search=%s&"                    % ("")
        weight_filter = "weight_filter=%d,%d&"   % (0, 1000)
        height_filter = "height_filter=%d,%d&"   % (0, 1000)
        base_xp_filter = "base_xp_filter=%d,%d&" % (0, 700)
        type_filter = "type_filter=%s"           % ("Invalidtype")
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + weight_filter + height_filter 
               + base_xp_filter + type_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        d = json.loads(response.get_data(as_text=True))
        
        cur_name = ""
        for entry in d["response"]:
            if cur_name == "":
                cur_name = entry["type"]
            else:
                self.assertLessEqual(entry["type"], cur_name)
                cur_name = entry["type"]

    # Testing sort and desc to see if they work correctly v2
    # @@lawson
    def test_pp_11(self):
        base_url = "/api/get_pokemon_page/?"
        page_num = "page_num=%d&"                % (1)
        num_per_page = "num_per_page=%d&"        % (1000)
        sort = "sort=%s&"                        % ("type")
        desc = "desc=%s&"                        % ("desc")
        search = "search=%s&"                    % ("")
        weight_filter = "weight_filter=%d,%d&"   % (0, 1000)
        height_filter = "height_filter=%d,%d&"   % (0, 1000)
        base_xp_filter = "base_xp_filter=%d,%d&" % (0, 700)
        type_filter = "type_filter=%s"           % ("Invalidtype")
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + weight_filter + height_filter 
               + base_xp_filter + type_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        d = json.loads(response.get_data(as_text=True))
        
        cur_name = ""
        for entry in d["response"]:
            if cur_name == "":
                cur_name = entry["type"]
            else:
                self.assertGreaterEqual(entry["type"], cur_name)
                cur_name = entry["type"]

    # Testing sort and desc to see if they work correctly v3
    # @@lawson
    def test_pp_12(self):
        base_url = "/api/get_pokemon_page/?"
        page_num = "page_num=%d&"                % (1)
        num_per_page = "num_per_page=%d&"        % (1000)
        sort = "sort=%s&"                        % ("ability_count")
        desc = "desc=%s&"                        % ("asc")
        search = "search=%s&"                    % ("")
        weight_filter = "weight_filter=%d,%d&"   % (0, 1000)
        height_filter = "height_filter=%d,%d&"   % (0, 1000)
        base_xp_filter = "base_xp_filter=%d,%d&" % (0, 700)
        type_filter = "type_filter=%s"           % ("Invalidtype")
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + weight_filter + height_filter 
               + base_xp_filter + type_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        d = json.loads(response.get_data(as_text=True))
        
        cur_name = ""
        for entry in d["response"]:
            if cur_name == "":
                cur_name = entry["ability_count"]
            else:
                self.assertGreaterEqual(entry["ability_count"], cur_name)
                cur_name = entry["ability_count"]

    # Testing sort and desc to see if they work correctly v4
    # @@lawson
    def test_pp_13(self):
        base_url = "/api/get_pokemon_page/?"
        page_num = "page_num=%d&"                % (1)
        num_per_page = "num_per_page=%d&"        % (1000)
        sort = "sort=%s&"                        % ("ability_count")
        desc = "desc=%s&"                        % ("desc")
        search = "search=%s&"                    % ("")
        weight_filter = "weight_filter=%d,%d&"   % (0, 1000)
        height_filter = "height_filter=%d,%d&"   % (0, 1000)
        base_xp_filter = "base_xp_filter=%d,%d&" % (0, 700)
        type_filter = "type_filter=%s"           % ("Invalidtype")
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + weight_filter + height_filter 
               + base_xp_filter + type_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        d = json.loads(response.get_data(as_text=True))
        
        cur_name = ""
        for entry in d["response"]:
            if cur_name == "":
                cur_name = entry["ability_count"]
            else:
                self.assertLessEqual(entry["ability_count"], cur_name)
                cur_name = entry["ability_count"]

    # Testing sort and desc to see if they work correctly v5
    # @@lawson
    def test_pp_14(self):
        base_url = "/api/get_pokemon_page/?"
        page_num = "page_num=%d&"                % (1)
        num_per_page = "num_per_page=%d&"        % (1000)
        sort = "sort=%s&"                        % ("moves_count")
        desc = "desc=%s&"                        % ("desc")
        search = "search=%s&"                    % ("")
        weight_filter = "weight_filter=%d,%d&"   % (0, 1000)
        height_filter = "height_filter=%d,%d&"   % (0, 1000)
        base_xp_filter = "base_xp_filter=%d,%d&" % (0, 700)
        type_filter = "type_filter=%s"           % ("Invalidtype")
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + weight_filter + height_filter 
               + base_xp_filter + type_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        d = json.loads(response.get_data(as_text=True))
        
        cur_name = ""
        for entry in d["response"]:
            if cur_name == "":
                cur_name = entry["moves_count"]
            else:
                self.assertLessEqual(entry["moves_count"], cur_name)
                cur_name = entry["moves_count"]
            

    # ------------------------
    # Type - Specific Call
    # ------------------------

    # @@lawson
    # Making sure we only get one response
    def test_ts_1(self):
        url = "/api/get_specific_type/1"
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(d["response"]), d["size"])
        self.assertEqual(len(d["response"]), 1)
        self.assertEqual(1, d["size"])

    # @@lawson
    # Making sure that the instances are correct
    def test_ts_2(self):
        url = "/api/get_specific_type/1"
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        d = json.loads(response.get_data(as_text=True))
        entry = d["response"][0]
        self.assertIsInstance(entry["generation"], str)
        self.assertIsInstance(entry["id"], int)
        self.assertIsInstance(entry["move_list"], list)
        self.assertIsInstance(entry["move_list_id"], list)
        self.assertIsInstance(entry["moves_count"], int)
        self.assertIsInstance(entry["name"], str)
        self.assertIsInstance(entry["no_damage_list"], list)
        self.assertIsInstance(entry["no_damage_list_id"], list)
        self.assertIsInstance(entry["pokemon_count"], int)
        self.assertIsInstance(entry["pokemon_list"], list)
        self.assertIsInstance(entry["pokemon_list_id"], list)

    # @@lawson
    # Making sure that the values are correct
    def test_ts_3(self):
        url = "/api/get_specific_type/1"
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        d = json.loads(response.get_data(as_text=True))
        entry = d["response"][0]
        self.assertEqual(entry["generation"], "1")
        self.assertEqual(entry["id"], 1)
        self.assertEqual(len(entry["move_list"]), len(entry["move_list_id"]))
        self.assertEqual(entry["moves_count"], len(entry["move_list"]))
        self.assertEqual(entry["name"], "normal")
        self.assertEqual(len(entry["no_damage_list"]),
                         len(entry["no_damage_list_id"]))
        self.assertEqual(len(entry["pokemon_list"]),
                         len(entry["pokemon_list_id"]))
        self.assertEqual(entry["pokemon_count"], len(entry["pokemon_list"]))

    # @@carter
    # Check that incorrect values gives no data back
    def test_ps_4(self):
        url = "/api/get_specific_type/19"
        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(d["response"]), 0)

    # ------------------------
    # Type - Range Call
    # ------------------------

    # @@carter
    def test_tr_1(self):
        url = "/api/get_type_range/5/14"
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(d["response"]), d["size"])
        self.assertEqual(len(d["response"]), 10)
        self.assertEqual(10, d["size"])

    # @@carter
    def test_tr_2(self):
        url = "/api/get_type_range/7/16"
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        d = json.loads(response.get_data(as_text=True))
        for entry in d["response"]:
            self.assertIsInstance(entry["generation"], str)
            self.assertIsInstance(entry["id"], int)
            self.assertIsInstance(entry["move_list"], list)
            self.assertIsInstance(entry["move_list_id"], list)
            self.assertIsInstance(entry["moves_count"], int)
            self.assertIsInstance(entry["name"], str)
            if("no_damage_list" in entry):  # some moves deal no damage to no pokemon
                self.assertIsInstance(entry["no_damage_list"], list)
            if("no_damage_list_id" in entry):
                self.assertIsInstance(entry["no_damage_list_id"], list)
            self.assertIsInstance(entry["pokemon_count"], int)
            self.assertIsInstance(entry["pokemon_list"], list)
            self.assertIsInstance(entry["pokemon_list_id"], list)

    # @@lawson
    def test_tr_3(self):
        url = "/api/get_type_range/3/18"
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        count = 3
        d = json.loads(response.get_data(as_text=True))
        for entry in d["response"]:
            self.assertEqual(entry["id"], count)
            count += 1

    # @@carter
    def test_tr_4(self):
        url = "/api/get_type_range/20/25"
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        d = json.loads(response.get_data(as_text=True))
        for entry in d["response"]:
            self.assertEqual(len(d["response"]), 0)

    # ------------------------
    # Type - Page Call
    # ------------------------

    # @@lawson
    def test_tp_1(self):
        base_url = "/api/get_type_page/?"
        page_num = "page_num=%d&"                           % (1)
        num_per_page = "num_per_page=%d&"                   % (10)
        sort = "sort=%s&"                                   % ("name")
        desc = "desc=%s&"                                   % ("desc")
        search = "search=%s&"                               % ("")
        generation_filter = "generation_filter=%d,%d&"      % (0, 8)
        moves_count_filter = "moves_count_filter=%d,%d&"    % (0, 150)
        pokemon_count_filter = "pokemon_count_filter=%d,%d" % (0, 200)
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + generation_filter + moves_count_filter 
               + pokemon_count_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(d["response"]), d["size"])
        self.assertEqual(len(d["response"]), 10)
        self.assertEqual(10, d["size"])

    # @@carter
    def test_tp_1_2(self):
        base_url = "/api/get_type_page/?"
        page_num = "page_num=%d&"                           % (1)
        num_per_page = "num_per_page=%d&"                   % (15)
        sort = "sort=%s&"                                   % ("name")
        desc = "desc=%s&"                                   % ("desc")
        search = "search=%s&"                               % ("")
        generation_filter = "generation_filter=%d,%d&"      % (0, 8)
        moves_count_filter = "moves_count_filter=%d,%d&"    % (0, 150)
        pokemon_count_filter = "pokemon_count_filter=%d,%d" % (0, 200)
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + generation_filter + moves_count_filter 
               + pokemon_count_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(d["response"]), d["size"])
        self.assertEqual(len(d["response"]), 15)
        self.assertEqual(15, d["size"])

    # @@fazal
    def test_tp_2(self):
        base_url = "/api/get_type_page/?"
        page_num = "page_num=%d&"                           % (1)
        num_per_page = "num_per_page=%d&"                   % (10)
        sort = "sort=%s&"                                   % ("name")
        desc = "desc=%s&"                                   % ("asc")
        search = "search=%s&"                               % ("")
        generation_filter = "generation_filter=%d,%d&"      % (0, 8)
        moves_count_filter = "moves_count_filter=%d,%d&"    % (0, 150)
        pokemon_count_filter = "pokemon_count_filter=%d,%d" % (0, 200)
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + generation_filter + moves_count_filter 
               + pokemon_count_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        cur_name = ""
        d = json.loads(response.get_data(as_text=True))
        for entry in d["response"]:
            if cur_name == "":
                cur_name = entry["name"]
            else:
                self.assertGreaterEqual(entry["name"], cur_name)
                cur_name = entry["name"]

    # @@lawson

    def test_tp_3(self):
        base_url = "/api/get_type_page/?"
        page_num = "page_num=%d&"                           % (1)
        num_per_page = "num_per_page=%d&"                   % (10)
        sort = "sort=%s&"                                   % ("name")
        desc = "desc=%s&"                                   % ("desc")
        search = "search=%s&"                               % ("")
        generation_filter = "generation_filter=%d,%d&"      % (0, 8)
        moves_count_filter = "moves_count_filter=%d,%d&"    % (0, 150)
        pokemon_count_filter = "pokemon_count_filter=%d,%d" % (0, 200)
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + generation_filter + moves_count_filter 
               + pokemon_count_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        cur_name = ""
        d = json.loads(response.get_data(as_text=True))
        for entry in d["response"]:
            if cur_name == "":
                cur_name = entry["name"]
            else:
                self.assertLessEqual(entry["name"], cur_name)
                cur_name = entry["name"]
    
    # @@lawson
    # Checking search capability
    def test_tp_4(self):
        base_url = "/api/get_type_page/?"
        page_num = "page_num=%d&"                           % (1)
        num_per_page = "num_per_page=%d&"                   % (1000)
        sort = "sort=%s&"                                   % ("name")
        desc = "desc=%s&"                                   % ("asc")
        search = "search=%s&"                               % ("Hello")
        generation_filter = "generation_filter=%d,%d&"      % (0, 8)
        moves_count_filter = "moves_count_filter=%d,%d&"    % (0, 150)
        pokemon_count_filter = "pokemon_count_filter=%d,%d" % (0, 200)
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + generation_filter + moves_count_filter 
               + pokemon_count_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(d["response"]), d["size"])
        self.assertEqual(len(d["response"]), 0)
        self.assertEqual(0, d["size"])

    #Further checking search
    # @@lawson
    def test_tp_5(self):
        search_query = "Pika"

        base_url = "/api/get_type_page/?"
        page_num = "page_num=%d&"                           % (1)
        num_per_page = "num_per_page=%d&"                   % (1000)
        sort = "sort=%s&"                                   % ("name")
        desc = "desc=%s&"                                   % ("asc")
        search = "search=%s&"                               % (search_query)
        generation_filter = "generation_filter=%d,%d&"      % (0, 8)
        moves_count_filter = "moves_count_filter=%d,%d&"    % (0, 150)
        pokemon_count_filter = "pokemon_count_filter=%d,%d" % (0, 200)
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + generation_filter + moves_count_filter 
               + pokemon_count_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(d["response"]), d["size"])
        self.assertEqual(len(d["response"]), 0)
        self.assertEqual(0, d["size"])

        for entry in d["response"]:
            self.assertTrue(search_query.lower() in entry["name"].lower())

    # Testing filtering on moves_count
    # @@lawson
    def test_tp_6(self):
        moves_count_range = (1,2)

        base_url = "/api/get_type_page/?"
        page_num = "page_num=%d&"                           % (1)
        num_per_page = "num_per_page=%d&"                   % (1000)
        sort = "sort=%s&"                                   % ("name")
        desc = "desc=%s&"                                   % ("asc")
        search = "search=%s&"                               % ("")
        generation_filter = "generation_filter=%d,%d&"      % (0, 8)
        moves_count_filter = "moves_count_filter=%d,%d&"    % (moves_count_range[0], moves_count_range[1])
        pokemon_count_filter = "pokemon_count_filter=%d,%d" % (0, 200)
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + generation_filter + moves_count_filter 
               + pokemon_count_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        
        d = json.loads(response.get_data(as_text=True))
        for entry in  d["response"]:
            self.assertLessEqual(entry["moves_count"], moves_count_range[1])
            self.assertGreaterEqual(entry["moves_count"], moves_count_range[0])

    # Testing a bad filtering call
    # @@lawson
    def test_tp_7(self):
        base_url = "/api/get_type_page/?"
        page_num = "page_num=%d&"                           % (1)
        num_per_page = "num_per_page=%d&"                   % (1000)
        sort = "sort=%s&"                                   % ("name")
        desc = "desc=%s&"                                   % ("asc")
        search = "search=%s&"                               % ("")
        generation_filter = "generation_filter=%d,%d&"      % (0, 8)
        moves_count_filter = "moves_count_filter=%d,%d&"    % (0, 1000)
        pokemon_count_filter = "pokemon_count_filter=%d,%d" % (20, 10)
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + generation_filter + moves_count_filter 
               + pokemon_count_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(d["response"]), 0)

    # Testing a bad filtering call v2
    # @@lawson
    def test_tp_8(self):
        base_url = "/api/get_type_page/?"
        page_num = "page_num=%d&"                           % (1)
        num_per_page = "num_per_page=%d&"                   % (1000)
        sort = "sort=%s&"                                   % ("name")
        desc = "desc=%s&"                                   % ("asc")
        search = "search=%s&"                               % ("")
        generation_filter = "generation_filter=%d,%d&"      % (0, 8)
        moves_count_filter = "moves_count_filter=%d,%d&"    % (700, 500)
        pokemon_count_filter = "pokemon_count_filter=%d,%d" % (0, 150)
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + generation_filter + moves_count_filter 
               + pokemon_count_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(d["response"]), 0)

    # Testing a bad filtering call v3
    # @@lawson
    def test_tp_9(self):
        base_url = "/api/get_type_page/?"
        page_num = "page_num=%d&"                           % (1)
        num_per_page = "num_per_page=%d&"                   % (1000)
        sort = "sort=%s&"                                   % ("name")
        desc = "desc=%s&"                                   % ("asc")
        search = "search=%s&"                               % ("")
        generation_filter = "generation_filter=%d,%d&"      % (0, 8)
        moves_count_filter = "moves_count_filter=%d,%d&"    % (0, 1000)
        pokemon_count_filter = "pokemon_count_filter=%d,%d" % (30, 10)
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + generation_filter + moves_count_filter 
               + pokemon_count_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(d["response"]), 0)

    # Testing sort and desc to see if they work correctly v1
    # @@lawson
    def test_tp_10(self):
        base_url = "/api/get_type_page/?"
        page_num = "page_num=%d&"                           % (1)
        num_per_page = "num_per_page=%d&"                   % (1000)
        sort = "sort=%s&"                                   % ("generation")
        desc = "desc=%s&"                                   % ("asc")
        search = "search=%s&"                               % ("")
        generation_filter = "generation_filter=%d,%d&"      % (0, 8)
        moves_count_filter = "moves_count_filter=%d,%d&"    % (0, 1000)
        pokemon_count_filter = "pokemon_count_filter=%d,%d" % (0, 150)
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + generation_filter + moves_count_filter 
               + pokemon_count_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        d = json.loads(response.get_data(as_text=True))
        
        cur_name = ""
        for entry in d["response"]:
            if cur_name == "":
                cur_name = entry["generation"]
            else:
                self.assertGreaterEqual(entry["generation"], cur_name)
                cur_name = entry["generation"]


    # Testing sort and desc to see if they work correctly v2
    # @@lawson
    def test_tp_11(self):
        base_url = "/api/get_type_page/?"
        page_num = "page_num=%d&"                           % (1)
        num_per_page = "num_per_page=%d&"                   % (1000)
        sort = "sort=%s&"                                   % ("generation")
        desc = "desc=%s&"                                   % ("desc")
        search = "search=%s&"                               % ("")
        generation_filter = "generation_filter=%d,%d&"      % (0, 8)
        moves_count_filter = "moves_count_filter=%d,%d&"    % (0, 1000)
        pokemon_count_filter = "pokemon_count_filter=%d,%d" % (0, 150)
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + generation_filter + moves_count_filter 
               + pokemon_count_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        d = json.loads(response.get_data(as_text=True))
        
        cur_name = ""
        for entry in d["response"]:
            if cur_name == "":
                cur_name = entry["generation"]
            else:
                self.assertLessEqual(entry["generation"], cur_name)
                cur_name = entry["generation"]

    # Testing sort and desc to see if they work correctly v3
    # @@lawson
    def test_tp_12(self):
        base_url = "/api/get_type_page/?"
        page_num = "page_num=%d&"                           % (1)
        num_per_page = "num_per_page=%d&"                   % (1000)
        sort = "sort=%s&"                                   % ("moves_count")
        desc = "desc=%s&"                                   % ("desc")
        search = "search=%s&"                               % ("")
        generation_filter = "generation_filter=%d,%d&"      % (0, 8)
        moves_count_filter = "moves_count_filter=%d,%d&"    % (0, 1000)
        pokemon_count_filter = "pokemon_count_filter=%d,%d" % (0, 150)
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + generation_filter + moves_count_filter 
               + pokemon_count_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        d = json.loads(response.get_data(as_text=True))
        
        cur_name = ""
        for entry in d["response"]:
            if cur_name == "":
                cur_name = entry["moves_count"]
            else:
                self.assertLessEqual(entry["moves_count"], cur_name)
                cur_name = entry["moves_count"]

    # Testing sort and desc to see if they work correctly v4
    # @@lawson
    def test_tp_13(self):
        base_url = "/api/get_type_page/?"
        page_num = "page_num=%d&"                           % (1)
        num_per_page = "num_per_page=%d&"                   % (1000)
        sort = "sort=%s&"                                   % ("moves_count")
        desc = "desc=%s&"                                   % ("asc")
        search = "search=%s&"                               % ("")
        generation_filter = "generation_filter=%d,%d&"      % (0, 8)
        moves_count_filter = "moves_count_filter=%d,%d&"    % (0, 1000)
        pokemon_count_filter = "pokemon_count_filter=%d,%d" % (0, 150)
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + generation_filter + moves_count_filter 
               + pokemon_count_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        d = json.loads(response.get_data(as_text=True))
        
        cur_name = ""
        for entry in d["response"]:
            if cur_name == "":
                cur_name = entry["moves_count"]
            else:
                self.assertGreaterEqual(entry["moves_count"], cur_name)
                cur_name = entry["moves_count"]

    # ------------------------
    # Move - Specific Call
    # ------------------------

    # @@lawson
    # Making sure we only get one response
    def test_ms_1(self):
        url = "/api/get_specific_move/1"
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(d["response"]), d["size"])
        self.assertEqual(len(d["response"]), 1)
        self.assertEqual(1, d["size"])

    # @@lawson
    # Making sure that the instances are correct
    def test_ms_2(self):
        url = "/api/get_specific_move/1"
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        d = json.loads(response.get_data(as_text=True))
        entry = d["response"][0]
        self.assertIsInstance(entry["accuracy"], int)
        self.assertIsInstance(entry["generation"], str)
        self.assertIsInstance(entry["id"], int)
        self.assertIsInstance(entry["image"], str)
        self.assertIsInstance(entry["name"], str)
        self.assertIsInstance(entry["pokemon_list"], list)
        self.assertIsInstance(entry["pokemon_list_id"], list)
        self.assertIsInstance(entry["power"], int)
        self.assertIsInstance(entry["pp"], int)
        self.assertIsInstance(entry["type"], str)
        self.assertIsInstance(entry["type_id"], int)

    # @@lawson
    # Making sure that the values are correct
    def test_ms_3(self):
        url = "/api/get_specific_move/1"
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        d = json.loads(response.get_data(as_text=True))
        entry = d["response"][0]
        self.assertEqual(entry["accuracy"], 100)
        self.assertEqual(entry["generation"], "1")
        self.assertEqual(entry["id"], 1)
        self.assertEqual(entry["image"],
                         "http://cdn2.bulbagarden.net/upload/2/2c/Pound_VIII.png")
        self.assertEqual(entry["name"], "pound")
        self.assertEqual(len(entry["pokemon_list"]),
                         len(entry["pokemon_list_id"]))
        self.assertEqual(entry["power"], 40)
        self.assertEqual(entry["pp"], 35)
        self.assertIn(
            entry["type"],
            [
                "normal",
                "fightying",
                "flying",
                "poison",
                "ground",
                "rock",
                "bug",
                "ghost",
                "steel",
                "fire",
                "water",
                "grass",
                "electric",
                "psychic",
                "ice",
                "dragon",
                "dark",
                "fairy"
            ],
        )
        self.assertEqual(entry["type_id"], 1)

    # @@carter
    # Check that incorrect values gives no data back
    def test_ps_4(self):
        url = "/api/get_specific_move/999"
        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(d["response"]), 0)

    # ------------------------
    # Move - Range Call
    # ------------------------

    # @@carter
    def test_mr_1(self):
        url = "/api/get_move_range/500/530"
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(d["response"]), d["size"])
        self.assertEqual(len(d["response"]), 31)
        self.assertEqual(31, d["size"])

    # @@carter
    def test_mr_2(self):
        url = "/api/get_move_range/151/161"
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        d = json.loads(response.get_data(as_text=True))
        for entry in d["response"]:
            self.assertIsInstance(entry["accuracy"], int)
            self.assertIsInstance(entry["generation"], str)
            self.assertIsInstance(entry["id"], int)
            if(entry["image"]):  # some moves have no images
                self.assertIsInstance(entry["image"], str)
            self.assertIsInstance(entry["name"], str)
            if("pokemon_list" in entry):  # some moves are learned by no pokemon
                self.assertIsInstance(entry["pokemon_list"], list)
            if("pokemon_list_id" in entry):
                self.assertIsInstance(entry["pokemon_list_id"], list)
            self.assertIsInstance(entry["power"], int)
            self.assertIsInstance(entry["pp"], int)
            self.assertIsInstance(entry["type"], str)
            self.assertIsInstance(entry["type_id"], int)

    # @@lawson
    def test_mr_3(self):
        url = "/api/get_move_range/314/324"
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        count = 314
        d = json.loads(response.get_data(as_text=True))
        for entry in d["response"]:
            self.assertEqual(entry["id"], count)
            count += 1

    # @@carter
    def test_mr_4(self):
        url = "/api/get_move_range/999989/999999"
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        d = json.loads(response.get_data(as_text=True))
        for entry in d["response"]:
            self.assertEqual(len(d["response"]), 0)

    # ------------------------
    # Move - Page Call
    # ------------------------

    # @@carter
    def test_mp_1(self):
        base_url = "/api/get_move_page/?"
        page_num = "page_num=%d&"                      % (1)
        num_per_page = "num_per_page=%d&"              % (10)
        sort = "sort=%s&"                              % ("name")
        desc = "desc=%s&"                              % ("asc")
        search = "search=%s&"                          % ("")
        generation_filter = "generation_filter=%d,%d&" % (0, 8)
        accuracy_filter = "accuracy_filter=%d,%d&"     % (0, 100)
        type_filter = "type_filter=%s"                 % ("")
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + generation_filter + accuracy_filter 
               + type_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(d["response"]), d["size"])
        self.assertEqual(len(d["response"]), 10)
        self.assertEqual(10, d["size"])

    # @@carter
    def test_mp_1_2(self):
        base_url = "/api/get_move_page/?"
        page_num = "page_num=%d&"                      % (1)
        num_per_page = "num_per_page=%d&"              % (15)
        sort = "sort=%s&"                              % ("name")
        desc = "desc=%s&"                              % ("asc")
        search = "search=%s&"                          % ("")
        generation_filter = "generation_filter=%d,%d&" % (0, 8)
        accuracy_filter = "accuracy_filter=%d,%d&"     % (0, 100)
        type_filter = "type_filter=%s"                 % ("")
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + generation_filter + accuracy_filter 
               + type_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(d["response"]), d["size"])
        self.assertEqual(len(d["response"]), 15)
        self.assertEqual(15, d["size"])

    # @@fazal
    def test_mp_2(self):

        base_url = "/api/get_move_page/?"
        page_num = "page_num=%d&"                      % (1)
        num_per_page = "num_per_page=%d&"              % (10)
        sort = "sort=%s&"                              % ("name")
        desc = "desc=%s&"                              % ("asc")
        search = "search=%s&"                          % ("")
        generation_filter = "generation_filter=%d,%d&" % (0, 8)
        accuracy_filter = "accuracy_filter=%d,%d&"     % (0, 100)
        type_filter = "type_filter=%s"                 % ("")
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + generation_filter + accuracy_filter 
               + type_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        cur_name = ""
        d = json.loads(response.get_data(as_text=True))
        for entry in d["response"]:
            if cur_name == "":
                cur_name = entry["name"]
            else:
                self.assertGreaterEqual(entry["name"], cur_name)
                cur_name = entry["name"]

    # @@lawson

    def test_mp_3(self):
        base_url = "/api/get_move_page/?"
        page_num = "page_num=%d&"                      % (30)
        num_per_page = "num_per_page=%d&"              % (10)
        sort = "sort=%s&"                              % ("name")
        desc = "desc=%s&"                              % ("desc")
        search = "search=%s&"                          % ("")
        generation_filter = "generation_filter=%d,%d&" % (0, 8)
        accuracy_filter = "accuracy_filter=%d,%d&"     % (0, 100)
        type_filter = "type_filter=%s"                 % ("")
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + generation_filter + accuracy_filter 
               + type_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        cur_name = ""
        d = json.loads(response.get_data(as_text=True))
        for entry in d["response"]:
            if cur_name == "":
                cur_name = entry["name"]
            else:
                self.assertLessEqual(entry["name"], cur_name)
                cur_name = entry["name"]

    # @@lawson
    # Checking search capability
    def test_mp_4(self):
        base_url = "/api/get_move_page/?"
        page_num = "page_num=%d&"                      % (1)
        num_per_page = "num_per_page=%d&"              % (1000)
        sort = "sort=%s&"                              % ("name")
        desc = "desc=%s&"                              % ("asc")
        search = "search=%s&"                          % ("Hello")
        generation_filter = "generation_filter=%d,%d&" % (0, 8)
        accuracy_filter = "accuracy_filter=%d,%d&"     % (0, 100)
        type_filter = "type_filter=%s"                 % ("")
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + generation_filter + accuracy_filter 
               + type_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(d["response"]), d["size"])
        self.assertEqual(len(d["response"]), 0)
        self.assertEqual(0, d["size"])

    #Further checking search
    # @@lawson
    def test_mp_5(self):
        search_query = "Pika"

        base_url = "/api/get_move_page/?"
        page_num = "page_num=%d&"                      % (1)
        num_per_page = "num_per_page=%d&"              % (1000)
        sort = "sort=%s&"                              % ("name")
        desc = "desc=%s&"                              % ("asc")
        search = "search=%s&"                          % (search_query)
        generation_filter = "generation_filter=%d,%d&" % (0, 8)
        accuracy_filter = "accuracy_filter=%d,%d&"     % (0, 100)
        type_filter = "type_filter=%s"                 % ("")
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + generation_filter + accuracy_filter 
               + type_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(d["response"]), d["size"])
        self.assertEqual(len(d["response"]), 2)
        self.assertEqual(2, d["size"])

        for entry in d["response"]:
            self.assertTrue(search_query.lower() in entry["name"].lower())

    # Testing filtering on accuray_filter
    # @@lawson
    def test_mp_6(self):
        accuracy_filter_range = (70,80)

        base_url = "/api/get_move_page/?"
        page_num = "page_num=%d&"                      % (1)
        num_per_page = "num_per_page=%d&"              % (20)
        sort = "sort=%s&"                              % ("name")
        desc = "desc=%s&"                              % ("asc")
        search = "search=%s&"                          % ("")
        generation_filter = "generation_filter=%d,%d&" % (0, 8)
        accuracy_filter = "accuracy_filter=%d,%d&"     % (accuracy_filter_range[0], accuracy_filter_range[1])
        type_filter = "type_filter=%s"                 % ("")
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + generation_filter + accuracy_filter 
               + type_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        d = json.loads(response.get_data(as_text=True))
        for entry in  d["response"]:
            self.assertLessEqual(entry["accuracy"], accuracy_filter_range[1])
            self.assertGreaterEqual(entry["accuracy"], accuracy_filter_range[0])

    # Testing a bad filtering call
    # @@lawson
    def test_mp_7(self):
        base_url = "/api/get_move_page/?"
        page_num = "page_num=%d&"                      % (1)
        num_per_page = "num_per_page=%d&"              % (20)
        sort = "sort=%s&"                              % ("name")
        desc = "desc=%s&"                              % ("asc")
        search = "search=%s&"                          % ("")
        generation_filter = "generation_filter=%d,%d&" % (0, 8)
        accuracy_filter = "accuracy_filter=%d,%d&"     % (0, 1000)
        type_filter = "type_filter=%s"                 % ("green")
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + generation_filter + accuracy_filter 
               + type_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(d["response"]), 0)

    # Testing a bad filtering call v2
    # @@lawson
    def test_mp_8(self):
        base_url = "/api/get_move_page/?"
        page_num = "page_num=%d&"                      % (1)
        num_per_page = "num_per_page=%d&"              % (20)
        sort = "sort=%s&"                              % ("name")
        desc = "desc=%s&"                              % ("asc")
        search = "search=%s&"                          % ("")
        generation_filter = "generation_filter=%d,%d&" % (0, 8)
        accuracy_filter = "accuracy_filter=%d,%d&"     % (400, 200)
        type_filter = "type_filter=%s"                 % ("")
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + generation_filter + accuracy_filter 
               + type_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(d["response"]), 0)

    # Testing a bad filtering call v3
    # @@lawson
    def test_mp_9(self):
        base_url = "/api/get_move_page/?"
        page_num = "page_num=%d&"                      % (1)
        num_per_page = "num_per_page=%d&"              % (20)
        sort = "sort=%s&"                              % ("name")
        desc = "desc=%s&"                              % ("asc")
        search = "search=%s&"                          % ("")
        generation_filter = "generation_filter=%d,%d&" % (0, 8)
        accuracy_filter = "accuracy_filter=%d,%d&"     % (0, 1000)
        type_filter = "type_filter=%s"                 % ("NotValid")
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + generation_filter + accuracy_filter 
               + type_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(d["response"]), 0)

    # Testing sort and desc to see if they work correctly v1
    # @@lawson
    def test_mp_10(self):
        base_url = "/api/get_move_page/?"
        page_num = "page_num=%d&"                      % (1)
        num_per_page = "num_per_page=%d&"              % (20)
        sort = "sort=%s&"                              % ("generation")
        desc = "desc=%s&"                              % ("asc")
        search = "search=%s&"                          % ("")
        generation_filter = "generation_filter=%d,%d&" % (0, 8)
        accuracy_filter = "accuracy_filter=%d,%d&"     % (0, 1000)
        type_filter = "type_filter=%s"                 % ("")
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + generation_filter + accuracy_filter 
               + type_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        d = json.loads(response.get_data(as_text=True))

        cur_name = ""
        for entry in d["response"]:
            if cur_name == "":
                cur_name = entry["generation"]
            else:
                self.assertGreaterEqual(entry["generation"], cur_name)
                cur_name = entry["generation"]

    # Testing sort and desc to see if they work correctly v2
    # @@lawson
    def test_mp_10(self):
        base_url = "/api/get_move_page/?"
        page_num = "page_num=%d&"                      % (1)
        num_per_page = "num_per_page=%d&"              % (20)
        sort = "sort=%s&"                              % ("generation")
        desc = "desc=%s&"                              % ("desc")
        search = "search=%s&"                          % ("")
        generation_filter = "generation_filter=%d,%d&" % (0, 8)
        accuracy_filter = "accuracy_filter=%d,%d&"     % (0, 1000)
        type_filter = "type_filter=%s"                 % ("")
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + generation_filter + accuracy_filter 
               + type_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        d = json.loads(response.get_data(as_text=True))

        cur_name = ""
        for entry in d["response"]:
            if cur_name == "":
                cur_name = entry["generation"]
            else:
                self.assertLessEqual(entry["generation"], cur_name)
                cur_name = entry["generation"]


    # Testing sort and desc to see if they work correctly v3
    # @@lawson
    def test_mp_11(self):
        base_url = "/api/get_move_page/?"
        page_num = "page_num=%d&"                      % (1)
        num_per_page = "num_per_page=%d&"              % (20)
        sort = "sort=%s&"                              % ("type")
        desc = "desc=%s&"                              % ("desc")
        search = "search=%s&"                          % ("")
        generation_filter = "generation_filter=%d,%d&" % (0, 8)
        accuracy_filter = "accuracy_filter=%d,%d&"     % (0, 1000)
        type_filter = "type_filter=%s"                 % ("")
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + generation_filter + accuracy_filter 
               + type_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        d = json.loads(response.get_data(as_text=True))

        cur_name = ""
        for entry in d["response"]:
            if cur_name == "":
                cur_name = entry["type"]
            else:
                self.assertLessEqual(entry["type"], cur_name)
                cur_name = entry["type"]

    # Testing sort and desc to see if they work correctly v4
    # @@lawson
    def test_mp_12(self):
        base_url = "/api/get_move_page/?"
        page_num = "page_num=%d&"                      % (1)
        num_per_page = "num_per_page=%d&"              % (20)
        sort = "sort=%s&"                              % ("type")
        desc = "desc=%s&"                              % ("asc")
        search = "search=%s&"                          % ("")
        generation_filter = "generation_filter=%d,%d&" % (0, 8)
        accuracy_filter = "accuracy_filter=%d,%d&"     % (0, 1000)
        type_filter = "type_filter=%s"                 % ("")
        
        url = (base_url + page_num + num_per_page + sort 
               + desc + search + generation_filter + accuracy_filter 
               + type_filter)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        d = json.loads(response.get_data(as_text=True))

        cur_name = ""
        for entry in d["response"]:
            if cur_name == "":
                cur_name = entry["type"]
            else:
                self.assertGreaterEqual(entry["type"], cur_name)
                cur_name = entry["type"]


if __name__ == "__main__":  # pragma: no cover
    main()
