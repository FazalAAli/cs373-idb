import json
import requests
from flask import jsonify
from main import app
import re as regex

raw_cache = {}
poke_api = "https://pokeapi.co/api/v2/"
card_api_url = "https://api.pokemontcg.io/v2/cards?q=name:"
header = {"X-Api-Key": "c44cfc0d-cea2-4fb5-b7f7-ddc86e05d795"}
bulbapedia_url = "https://bulbapedia.bulbagarden.net/w/api.php?"


def api_get_data_raw(category, n):
    '''
    Helper function that actually gets the data for a speified pokemon
    '''
    if category not in raw_cache:
        raw_cache[category] = {}

    category_raw_cache = raw_cache[category]

    if n in category_raw_cache:
        return True

    data = requests.get(poke_api+f"/{category}/{n}")

    if data.status_code == 200:
        # pokemon exists we can get detailed data
        category_raw_cache[n] = (data.text)
        return True
    else:
        return False


def filter_value(unparsed_raw_json_data, function_to_use):
    '''
    Helper function to convert raw data to filtered value using the mapping functions defined in mapping.py
    '''
    temp_dict = json.loads(unparsed_raw_json_data)
    response_dict = function_to_use(temp_dict)

    # Add general values
    if "name" in temp_dict:
        response_dict["name"] = temp_dict["name"]

    if "id" in temp_dict:
        response_dict["id"] = temp_dict["id"]

    return response_dict


@ app.route("/api/api/get_<category>_range/<int:start>-<int:end>")
def api_get_range(category, start, end):
    '''
    Returns a segment of the category because there are might be too many and it
    takes too long to process. Helpful for pagination or table data
    '''
    # Ensure start and end assertions
    if end < start:
        start, end = end, start

    if start < 0:
        return "Can't have id less than 0"
    # Get the data and store in holder
    holder = {}
    for i in range(start, end+1):
        api_get_data_raw(category, i)
        holder[i] = raw_cache[category][i]

    # Process the values to get only the ones we care about
    response = []
    function_to_use = api_function_mappig[category]
    for value in holder:
        repsponse_dict = filter_value(holder[value], function_to_use)
        response.append(repsponse_dict)

    return jsonify({
        "size": len(response),
        "response": response
    })


@ app.route("/api/api/get_specific_<category>/<int:id>")
def api_get_specific(category, id):
    '''
    Returns cateogry item based on id provided
    '''
    return api_get_range(category, id, id)


def api_type_dict_mapping(temp_dict):
    response_dict = {}
    response_dict['generation'] = temp_dict['generation']["name"]

    if len(temp_dict["damage_relations"]["no_damage_to"]) > 0:
        response_dict['no_damage_to_name'] = [x["name"]
                                              for x in temp_dict["damage_relations"]["no_damage_to"]]
        response_dict['no_damage_to_id'] = [convert_poke_link_to_id(
            x["url"]) for x in temp_dict["damage_relations"]["no_damage_to"]]

    if len(temp_dict["damage_relations"]["double_damage_to"]) > 0:
        response_dict['double_damage_to_name'] = [x["name"]
                                                  for x in temp_dict["damage_relations"]["double_damage_to"]]

        response_dict['double_damage_to_id'] = [convert_poke_link_to_id(
            x["url"]) for x in temp_dict["damage_relations"]["double_damage_to"]]

    response_dict['pokemon_count'] = len(temp_dict['pokemon'])
    response_dict['pokemon_list'] = [x['pokemon']['name']
                                     for x in temp_dict['pokemon']]
    response_dict['pokemon_list_id'] = [convert_poke_link_to_id(x['pokemon']['url'])
                                        for x in temp_dict['pokemon']]

    response_dict['moves_count'] = len(temp_dict['moves'])
    response_dict['move_list'] = [x['name']
                                  for x in temp_dict['moves']]
    response_dict['move_list_id'] = [convert_poke_link_to_id(x['url'])
                                     for x in temp_dict['moves']]

    return response_dict


def api_moves_dict_mapping(temp_dict):
    response_dict = {}
    response_dict['generation'] = temp_dict['generation']['name']
    response_dict['accuracy'] = temp_dict['accuracy']
    response_dict['pp'] = temp_dict['pp']
    response_dict['power'] = temp_dict['power']
    response_dict['pokemon_list'] = [x['name']
                                     for x in temp_dict['learned_by_pokemon']]

    response_dict['pokemon_list_id'] = [convert_poke_link_to_id(x['url'])
                                        for x in temp_dict['learned_by_pokemon']]
    response_dict['type'] = temp_dict['type']['name']
    response_dict['type_id'] = convert_poke_link_to_id(
        temp_dict['type']['url'])

    # Get image
    bulbapedia_name_list = temp_dict["name"].split("-")
    bulbapedia_name = ""
    for name in bulbapedia_name_list:
        bulbapedia_name += name.capitalize() + " "
    bulbapedia_name += "(move)"

    image_json = requests.get(
        bulbapedia_url + f'action=query&format=json&prop=pageimages&titles={bulbapedia_name}&piprop=original')
    image_data = json.loads(image_json.text)
    try:
        pages = image_data["query"]["pages"]
        image = pages[next(iter(pages))]["original"]["source"]
        response_dict["image"] = image
    except:
        print("Exception")

    return response_dict


def api_pokemon_dict_mapping(temp_dict):
    response_dict = {}
    response_dict['sprite'] = temp_dict['sprites']['front_default']

    response_dict['moves_count'] = len(temp_dict['moves'])
    response_dict['move_list'] = [x['move']['name']
                                  for x in temp_dict['moves']]
    response_dict['move_list_id'] = [convert_poke_link_to_id(x['move']['url'])
                                     for x in temp_dict['moves']]

    response_dict['type'] = temp_dict['types'][0]['type']['name']
    response_dict['type_id'] = convert_poke_link_to_id(
        temp_dict['types'][0]['type']['url'])

    response_dict['ability_count'] = len(temp_dict['abilities'])

    response_dict['weight'] = temp_dict["weight"]
    response_dict['height'] = temp_dict["height"]
    response_dict['base_xp'] = temp_dict["base_experience"]

    sum = 0
    for stat in temp_dict["stats"]:
        sum += stat["base_stat"]

    response_dict['base_stat_total'] = sum

    # Get card
    try:
        card_json = requests.get(
            card_api_url + temp_dict['name'], headers=header).text
        card_data = json.loads(card_json)
        response_dict["card_image"] = card_data["data"][0]["images"]["large"]
    except:
        print("Excpetion getting card")
    return response_dict


def convert_poke_link_to_id(s):
    matched = regex.findall("[0-9]+", s)
    return matched[-1]


api_function_mappig = {'type': api_type_dict_mapping,
                       'move': api_moves_dict_mapping,
                       'pokemon': api_pokemon_dict_mapping}
