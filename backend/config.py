import os

if os.environ.get("PROD") == "True":
    config_dict = {
        "DEBUG": False,
        "SQLALCHEMY_DATABASE_URI": "postgresql:///data",
        "ROOT_PATH": "/home/ubuntu/cs373-idb/"
    }
else:
    config_dict = {
        "DEBUG": True,
        "SQLALCHEMY_DATABASE_URI": "sqlite:///data.db",
        "ROOT_PATH": os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
    }

config_dict["CACHE_TYPE"] = "SimpleCache"
config_dict["CACHE_DEFAULT_TIMEOUT"] = 0
config_dict["SQLALCHEMY_TRACK_MODIFICATIONS"] = True
config_dict["MSEARCH_INDEX_NAME"] = "msearch"
config_dict["MSEARCH_BACKEND"] = 'whoosh'
config_dict["MSEARCH_ENABLE"] = True
config_dict["MSEARCH_PRIMARY_KEY"] = 'id'
