backend_tests: backend/tests/backend_tests.py
	$(info Running backend tests)
	coverage run --branch backend/tests/backend_tests.py > backend/tests/testing_output/backend_tests.out 2>&1
	cat backend/tests/testing_output/backend_tests.out

db_tests: backend/tests/db_tests.py
	$(info Running db tests)
	coverage run --branch backend/tests/db_tests.py > backend/tests/testing_output/db_tests.out 2>&1
	cat backend/tests/testing_output/db_tests.out

postman_tests:
	$(info Running postman tests)
	newman run postman_tests_v2.json > backend/tests/testing_output/postman_tests.out 2>&1
	cat backend/tests/testing_output/postman_tests.out

frontend_tests: frontend/tests/tests.py
	$(info Running frontend tests)
	coverage run --branch frontend/tests/tests.py > frontend/tests/testing_output/tests.out 2>&1
	cat frontend/tests/testing_output/tests.out

tests: backend_tests db_tests postman_tests

.PHONY: backend
backend: backend/*
	python backend/main.py

.PHONY: frontend
frontend: frontend/*
	cd frontend && npm start

dependencies: backend/requirements.txt frontend/package.json frontend/requirements.txt
	python -m pip install -r backend/requirements.txt
	cd frontend && npm install
	python -m pip install -r frontend/requirements.txt

build_database: backend/build_database.py
	python backend/build_database.py

clean:
	rm backend/testing_output/*.out
