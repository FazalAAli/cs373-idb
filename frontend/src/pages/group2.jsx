import { useEffect, useState } from "react";
import NavBar from "../components/navbar";
import PageTemplate from "../components/template";
import { color_dict } from ".";
var graph1, graph2, graph3;

function Group2Page() {

    const [data_pulled, setData] = useState();
    const [loaded, setLoaded] = useState(false)

    useEffect(() => {
        var url = "https://trackmycandidate.com/api/representatives?page_size=99999"
        fetch(url)
            .then(res => res.json())
            .then(
                (result) => {
                    setData(result["data"]);
                    setLoaded(true);
                }
            )
    }, []);


    useEffect(() => {

        var counter = {}
        var bills_per_party = {}
        var candidates_by_jurisdiction = {}
        if (loaded) {

            for (var i = 0; i < data_pulled.length; i++) {
                var current_party = data_pulled[i]["party"]

                if (Object.keys(counter).indexOf(current_party) != -1) {
                    counter[current_party] += 1
                    bills_per_party[current_party] += data_pulled[i]["bills"].length;

                } else {
                    counter[current_party] = 1
                    bills_per_party[current_party] = data_pulled[i]["bills"].length;
                }

                var current_jurisdiction = data_pulled[i]["jurisdiction"]["name"]

                if (Object.keys(candidates_by_jurisdiction).indexOf(current_jurisdiction) != -1) {
                    candidates_by_jurisdiction[current_jurisdiction] += 1
                } else {
                    candidates_by_jurisdiction[current_jurisdiction] = 1
                }
            }
            console.log("Loaded actual data from API!")
            graph1.destroy();
            graph2.destroy();
            graph3.destroy();


        } else {
            //This data was scraped from their webpage prior to them setting up their API
            //It was scraped using python and requests module and I decided to keep it in because I noticed their API was
            //Unresponsive periodically
            counter = {
                "Republican": 571,
                "Independent": 5,
                "Democratic": 342,
                "Democratic- Farmer - Labor": 129,
            }

            bills_per_party = {
                "Republican": 24467,
                "Independent": 153,
                "Democratic": 20808,
                "Democratic- Farmer - Labor": 5237,

            }

            candidates_by_jurisdiction = {
                "California": 122,
                "Georgia": 238,
                "Michigan": 150,
                "Minnesota": 253,
                "Oklahoma": 151,
                "Wisconsin": 133
            }

        }


        var data = {
            labels: Object.keys(counter),
            datasets: [{
                backgroundColor: [
                    'rgba(255, 99, 132, 0.8)',
                    'rgba(255, 206, 86, 0.8)',
                    'rgba(54, 162, 235, 0.8)',
                    'rgba(54, 162, 235, 0.8)'
                ],

                data: Object.values(counter),
            }]
        };

        graph1 = new window.Chart(
            document.getElementById('myChart'),
            {
                type: 'bar',
                data,
                options: {
                    plugins: {
                        title: {
                            display: true,
                            text: 'Party Background by candidate',
                            font: {
                                size: 18
                            }
                        },
                        legend: {
                            display: false
                        }
                    },
                    scales: {
                        x: {
                            title: {
                                display: true,
                                text: "Parties",
                                font: {
                                    size: 14
                                }
                            }
                        },
                        y: {
                            title: {
                                display: true,
                                text: "Number of Reps",
                                font: {
                                    size: 14
                                }
                            }
                        }
                    }
                }
            }
        );


        var data = {
            labels: Object.keys(bills_per_party),
            datasets: [{
                backgroundColor: [
                    'rgba(255, 99, 132, 0.8)',
                    'rgba(255, 206, 86, 0.8)',
                    'rgba(54, 162, 235, 0.8)',
                    'rgba(54, 162, 235, 0.8)'
                ],

                data: Object.values(bills_per_party),
            }]
        };


        graph2 = new window.Chart(
            document.getElementById('myChart1'),
            {
                type: 'bar',
                data,
                options: {
                    plugins: {
                        title: {
                            display: true,
                            text: "Number of bills by party",
                            font: {
                                size: 18
                            }
                        },
                        legend: {
                            display: false
                        }
                    },
                    scales: {
                        x: {
                            title: {
                                display: true,
                                text: "Parties",
                                font: {
                                    size: 14
                                }
                            }
                        },
                        y: {
                            title: {
                                display: true,
                                text: "Number of Bills",
                                font: {
                                    size: 14
                                }
                            }
                        }
                    }
                }
            }
        );

        var data = {
            labels: Object.keys(candidates_by_jurisdiction),

            datasets: [{
                backgroundColor: [
                    'purple',
                ],

                data: Object.values(candidates_by_jurisdiction),
            }]
        };


        graph3 = new window.Chart(
            document.getElementById('myChart2'),
            {
                type: 'bar',
                data,
                options: {
                    plugins: {
                        title: {
                            display: true,
                            text: "Number of candidates per Jurisdiction",
                            font: {
                                size: 18
                            }
                        },
                        legend: {
                            display: false
                        }
                    },
                    scales: {
                        x: {
                            title: {
                                display: true,
                                text: "Jurisdictions",
                                font: {
                                    size: 14
                                }
                            }
                        },
                        y: {
                            title: {
                                display: true,
                                text: "Number of Reps",
                                font: {
                                    size: 14
                                }
                            }
                        }
                    }
                }
            }
        );
    }, [loaded])

    return (
        <PageTemplate color={color_dict["snor"]}>
            <center>
                <NavBar></NavBar>
                <div className="col-11 col-lg-6 table-data-container" >
                    <div className="col-12">
                        <canvas id="myChart" className="p-4"></canvas>
                        <canvas id="myChart1" className="p-4"></canvas>
                        <canvas id="myChart2" className="p-4"></canvas>
                    </div>
                </div>
            </center>
        </PageTemplate>
    );


};
export default Group2Page;