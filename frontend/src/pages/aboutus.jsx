import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleNotch } from '@fortawesome/free-solid-svg-icons'
import JustifiedFlex from "../components/components";
import PageTemplate from "../components/template";
import fazal from "../assets/imgs/fazal.jpg"
import NavBar from "../components/navbar";

function Person(props) {
  const user = props.user;

  return (

    <div className="card col-12 col-md-3 m-3 rounded shadow-lg">
      <img src={user.img} className="card-img-top img-fluid" style={{ height: '300px', objectFit: 'cover' }} alt="" />
      <div className="card-body">
        <h5 className="card-title">{user.fullname}</h5>
        <span className="badge bg-info text-dark mx-1 mx-md-0 mx-lg-1">Commits: {user.commits >= 0 ? user.commits : <FontAwesomeIcon icon={faCircleNotch} spin />}</span>
        <span className="badge bg-info text-dark mx-1 mx-md-0 mx-lg-1">Issues: {user.issues >= 0 ? user.issues : <FontAwesomeIcon icon={faCircleNotch} spin />}</span>
        <span className="badge bg-info text-dark mx-1">Tests: {user.tests >= 0 ? user.tests : <FontAwesomeIcon icon={faCircleNotch} spin />}</span>
        <p className="card-text">{user.bio}</p>
        <p className="card-text"><b>Responsibilities: </b> {user.responsibilities}</p>
        {user.url && <a href={user.url} target="_blank" rel="noreferrer" className="btn btn-primary">See website</a>}
      </div>
    </div>
  );
}

function Developers() {
  const bio_data = {
    'trevor': {
      fullname: 'Trevor Nguyen',
      bio: <p>I'm interested in studying neuroscience or medicine or both. I'm very fascinated by the role of technology to enhance medicine in an ethical and patient-focused way. I don't know what a Pokemon is, but <a href="/pokemons/6">Charizard</a> seems cool.</p>,
      url: "https://neuron.bio/",
      img: "https://neuron.bio/assets/me-on-a-bench.jpg",
      responsibilities: "Front end"
    },
    'carter': {
      fullname: 'Carter Boclair',
      bio: <p>I enjoy programming games and occasionally do game jams  in my free time. I also love plants and keep a few bonsai trees. Favorite Pokemon: <a href="/pokemons/233">Porygon 2</a></p>,
      url: "https://www.cs.utexas.edu/users/carb/",
      img: "https://gitlab.com/FazalAAli/cs373-idb/uploads/43a5cc91f36db83bf7fd993239fb4a26/IMG_3895.jpg",
      responsibilities: "Front end"
    },
    'lawson': {
      fullname: "Chris Lawson",
      bio: <p>I like to spend time walking my dog outside and gaming. I have a passion for manipulating data and have competed in a couple hackathons in that area. My favorite Pokémon is <a href="/pokemons/66">Machop</a>.</p>,
      url: "https://www.cs.utexas.edu/users/lawso101/",
      img: "https://gitlab.com/FazalAAli/cs373-idb/uploads/682408ba29d8ed991c68ef74b2cb687b/6C74A38B-B73B-49FE-9314-C8313C68D178.jpeg",
      responsibilities: "Front end"
    },
    'rice': {
      fullname: "Chris Rice",
      bio: <p>I'm an aspiring video game developer hoping to make games that tell great stories. In my free time, I like to play video games and take pictures. My favourite pokemon is <a href="/pokemons/609">Chandelure</a>.</p>,
      url: "https://www.cs.utexas.edu/~cjr3637/",
      img: "https://gitlab.com/FazalAAli/cs373-idb/uploads/c5f49d281e127d46838c02287500e1c9/img-1819.jpg",
      responsibilities: "Front end"
    },
    'fazal': {
      fullname: "Fazal Ali",
      bio: <p>I like looking at data and analyzing it. My favorite hobbies are sleeping and eating and sometimes both at the same time. My favorite pokemon is <a href="/pokemons/143">Snorlax</a>.</p>,
      url: "https://www.fazalali.me/",
      img: fazal,
      responsibilities: "Back end"
    }
  }

  const [users, setUsers] = useState(bio_data);
  const [totalCommits, setTotalCommits] = useState(0);
  const [totalIssues, setTotalIssues] = useState(0);
  const [totalUnitTests, setTotalUnitTests] = useState(0);
  useEffect(() => {
    const fetchCommits = async () => {
      var data = []
      let xNextPage = "1";
      while (xNextPage) {
        const response = await fetch("https://gitlab.com/api/v4/projects/27528942/repository/commits?per_page=100&page=" + xNextPage)
        const data_json = await response.json()
        xNextPage = response.headers.get('x-next-page')

        data = data.concat(data_json)
      }
      return data
    }

    const fetchIssues = async () => {
      var data = []
      let xNextPage = "1";
      while (xNextPage) {
        const response = await fetch("https://gitlab.com/api/v4/projects/27528942/issues?per_page=100&page=" + xNextPage)
        const data_json = await response.json()
        xNextPage = response.headers.get('x-next-page')

        data = data.concat(data_json)
      }
      return data
    }

    const fetchTests = async () => {
      var response = await fetch(`${process.env.REACT_APP_API_LINK}/api/get_num_tests/`)
      var data_json = await response.json()
      return data_json
    }

    const getCommits = async () => {
      const data_json = await fetchCommits()
      setTotalCommits(data_json.length)

      const issues_data = await fetchIssues()
      setTotalIssues(issues_data.length)

      const test_data = await fetchTests()
      setTotalUnitTests(() => {
        var sum = 0
        var key;
        for (key in test_data) {
          sum += test_data[key]
        }
        return sum
      })

      const updatedUsers = { ...users }

      Object.keys(users).forEach((name) => {
        const myCommits = data_json.filter((item) => item['author_name'].toLowerCase().includes(name))
        const myIssues = issues_data
          .map((issue) => issue['assignees'])
          .filter((issue_assignees) => {
            return issue_assignees.filter((assignee) => assignee['name'].toLowerCase().includes(name)).length > 0
          })

        updatedUsers[name] = {
          ...users[name],
          'commits': myCommits.length,
          'issues': myIssues.length,
          'tests': test_data[name]
        };
      })

      setUsers(updatedUsers);
    }
    getCommits();
  }, []);
  return (
    <div className="container">
      <JustifiedFlex>
        {Object.values(users).map(person => <Person user={person} />)}
      </JustifiedFlex>
      <center>
        <h3 className="mt-5">Total commits: {totalCommits || "241"} | Total Issues: {totalIssues || "70"} | Total UnitTests: {totalUnitTests || "99"}</h3>
      </center>

    </div >
  );
};


//Functional Component 
const AboutUs = () => {
  return (
    <PageTemplate color='#FFFFFF'>
      <center><NavBar></NavBar></center>
      <Developers />

      <br></br>
      <center><h3 className="m-3">Important Links and Tools</h3>
        <a target="_blank" rel="noreferrer" className="btn btn-primary m-3" href="https://gitlab.com/FazalAAli/cs373-idb"><h5>Gitlab Repository</h5></a>
        <a target="_blank" rel="noreferrer" className="btn btn-primary m-3" href="https://gitlab.com/FazalAAli/cs373-idb/-/wikis/Phase-3-Technical-Report-01.-Introduction"><h5>Technical Report</h5></a>
        <a target="_blank" rel="noreferrer" className="btn btn-primary m-3" href="https://gitlab.com/FazalAAli/cs373-idb/-/issues"><h5>Gitlab Issues</h5></a>
        <a target="_blank" rel="noreferrer" className="btn btn-primary m-3" href="https://speakerdeck.com/clawson101/cs373-pokedex-presentation"><h5>SpeakerDeck</h5></a>
        <br></br>
        <a target="_blank" rel="noreferrer" className="btn btn-primary m-3" href="https://documenter.getpostman.com/view/16465182/Tzm2JxcH"><h5>API Postman Collection</h5></a>
        <a target="_blank" rel="noreferrer" className="btn btn-primary m-3" href="https://www.getpostman.com/collections/16e9f478a230781f1edc"><h5>API Postman Collection JSON</h5></a>
        <br></br>
        <a target="_blank" rel="noreferrer" className="btn btn-primary m-3" href="https://www.pokeapi.co"><h5>PokeAPI</h5></a>
        <a target="_blank" rel="noreferrer" className="btn btn-primary m-3" href="https://pokemontcg.io/"><h5>PokeTCG - Poke cards</h5></a>
        <a target="_blank" rel="noreferrer" className="btn btn-primary m-3" href="https://bulbapedia.bulbagarden.net/wiki/Main_Page"><h5>Bulbapedia</h5></a>
        <br></br>
        <a target="_blank" rel="noreferrer" className="btn btn-primary m-3" href="https://www.deployhq.com/"><h5>DeployHQ</h5></a>
        <a target="_blank" rel="noreferrer" className="btn btn-primary m-3" href="https://www.nginx.com/"><h5>Nginx</h5></a>
        <a target="_blank" rel="noreferrer" className="btn btn-primary m-3" href="https://gunicorn.org"><h5>Gunicorn</h5></a>
        <br></br>
        <a target="_blank" rel="noreferrer" className="btn btn-primary m-3" href="https://code.visualstudio.com"><h5>VS Code</h5></a>
        <a target="_blank" rel="noreferrer" className="btn btn-primary m-3" href="https://discord.com"><h5>Discord</h5></a>
        <br></br>
        <button className="btn btn-primary m-1" onClick={() => runTests()}><h5>Run Tests</h5></button>
        <br></br>
        <div className="d-none my-3 col-6 individual-data-container pre-line" id="TestResults">test results data</div>
        <br></br>
        <button className="btn btn-primary" onClick={() => showDiv("dataCollectionInfo")}><h5>How the data was collected</h5></button>
        <br></br>
        <div className="d-block my-2 col-6 individual-data-container" id="dataCollectionInfo">
          <p className="p-4">
            We made requests to pokeapi for the data, pokemon TCG for the cards, and bulbapedia for all the other pictures (specifically move pictures).
            To do this programatically, we started with the types - because all pokemons and moves have a type.
            Then, we made requests for all the pokemon and move children of that type. We also got the data for the children of the children
            to ensure that our dataset was complete and all mappings that stemp from a type's child are established.
            This had the added benefit of speeding up the data collection as move and pokemon have a many to many mapping
            so scraping their children resulted in much of the required data already being pulled beforehand.
            <br></br><br></br>
            To filter the data, we converted the raw data from pokeapi into json, then used mappings to convert the raw data into
            data we wanted to display - this list of attributes and conversions can be found in api_endpoints.py and build_database.py
          </p>
        </div>

      </center>
    </PageTemplate>
  );
};

async function runTests() {
  var test_div = document.getElementById("TestResults");
  showDiv("TestResults")

  if (test_div.classList.contains("d-block")) {
    test_div.innerHTML = "Loading... \n The results take about 2 minutes to run"
    var results = await fetch(`${process.env.REACT_APP_API_LINK}/api/run_tests/`)
    var results_data = await results.json();
    results_data = results_data["response"]
    test_div.innerHTML = results_data
  }
}

function showDiv(identifier) {
  var test_div = document.getElementById(identifier);
  test_div.classList.toggle("d-none");
  test_div.classList.toggle("d-block");
}

export default AboutUs;