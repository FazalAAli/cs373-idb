import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import PageTemplate from "../components/template";
import NavBar from "../components/navbar";
import SingleDataContainer from "../components/single_data_cointainer";
import HideableDiv from "../components/hideable_div";
import { color_dict } from ".";
import get_full_list from "../components/get_full_list";


//Functional Component
function MovePage() {
    const [data, setData] = useState({});
    const { id } = useParams();
    const [isShowing, setIsShowing] = useState(false);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_LINK}/api/get_specific_move/${id}`)
            .then(res => res.json())
            .then(
                (result) => {
                    setData(result["response"][0]);
                }
            )
    }, [])

    function powerFormatter(power) {
        if (power != null) {
            return `${power}`
        }
        return `${'N/A'}`
    }

    var full_list = get_full_list(data['pokemon_list'], data['pokemon_list_id'], "pokemons");
    return (
        <PageTemplate color={color_dict["bulb"]} >
            <center><NavBar num={10} desc="asc"></NavBar></center>

            <SingleDataContainer>
                <h1 className="displaypageTitle">{data['name']}</h1>
                <hr></hr>
                <img className="align-self-center col-8 img img-responsive" src={data['image']} alt="Pokemon Move" /> < br />
                <span className="individual-data-header">Name: </span>{data['name']} <br />
                <span className="individual-data-header">Type: </span><Link to={`/types/${data['type_id']}`}>{data['type']}</Link><br />
                <span className="individual-data-header">Generation: </span>{data['generation']} <br />
                <span className="individual-data-header">Accuracy: </span>{data['accuracy']} <br />
                <span className="individual-data-header">Power Points (PP): </span>{data['pp']}<br />
                <span className="individual-data-header">Power: </span>{powerFormatter(data['power'])}<br />
                <HideableDiv setter={setIsShowing} trackingVariable={isShowing}
                    showMessage="Show Pokemon" hideMessage="Hide Pokemon">
                    {full_list}
                </HideableDiv>
            </SingleDataContainer>
        </PageTemplate >
    );

};
export default MovePage;