import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import PageTemplate from "../components/template";
import NavBar from "../components/navbar";
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleNotch } from '@fortawesome/free-solid-svg-icons'
import multi_word_search_alert_box_update from "../components/multi_word_search";

function SearchResultsPage() {
    const [typeData, setTypeData] = useState({});
    const [moveData, setMoveData] = useState({});
    const [pokemonData, setPokemonData] = useState({});
    const [page_num, setPageNum] = useState(1)
    const [num_per_page, setNumPerPage] = useState(1000)
    const [sort, setSort] = useState("name")
    const [desc, setDesc] = useState("asc")

    var type_url = new URL(`${process.env.REACT_APP_API_LINK}/api/get_type_page/`)
    var move_url = new URL(`${process.env.REACT_APP_API_LINK}/api/get_move_page/`)
    var pokemon_url = new URL(`${process.env.REACT_APP_API_LINK}/api/get_pokemon_page/`)
    var pokemon_mark_instance = new window.Mark(document.querySelector(".pokemon-table-data"));
    var move_mark_instance = new window.Mark(document.querySelector(".move-table-data"));
    var type_mark_instance = new window.Mark(document.querySelector(".type-table-data"));

    var options = {
        noDataText: "No data matches criteria"
    };

    const queryParams = new URLSearchParams(window.location.search);
    const search = queryParams.get('query');

    useEffect(() => {
        multi_word_search_alert_box_update("multiWordSearchAlert", search)
        setPokemonData({});

        var params = {
            page_num, num_per_page, sort, desc, search
        }

        Object.keys(params).forEach(key => pokemon_url.searchParams.append(key, params[key]))

        fetch(pokemon_url)
            .then(res => res.json())
            .then(
                (result) => {
                    setPokemonData(result);
                }
            )

        setTypeData({});

        Object.keys(params).forEach(key => type_url.searchParams.append(key, params[key]))

        fetch(type_url)
            .then(res => res.json())
            .then(
                (result) => {
                    setTypeData(result);
                }
            )

        setMoveData({});

        Object.keys(params).forEach(key => move_url.searchParams.append(key, params[key]))

        fetch(move_url)
            .then(res => res.json())
            .then(
                (result) => {
                    setMoveData(result);
                }
            )

    }, [search]);

    useEffect(() => {
        pokemon_mark_instance.mark(search, { "className": "highlight" });
        move_mark_instance.mark(search, { "className": "highlight" });
        type_mark_instance.mark(search, { "className": "highlight" });
    }, [pokemonData, moveData, typeData])

    return <PageTemplate>
        <center><NavBar></NavBar>
            <div id="multiWordSearchAlert" className="alert alert-primary d-none col-6"></div>
            <h2>Pokemons</h2>

            <div className="col-6 pokemon-table-data">
                {pokemonData["response"] ?
                    <BootstrapTable hover striped data={pokemonData["response"]} options={options} id="table-data">
                        <TableHeaderColumn isKey dataFormat={pokemon_name_link} dataField='name'>Name</TableHeaderColumn>
                        <TableHeaderColumn dataField='type' >Type</TableHeaderColumn>
                        <TableHeaderColumn dataField='weight' >Weight(kg)</TableHeaderColumn>
                        <TableHeaderColumn dataField='height' >Height(m)</TableHeaderColumn>
                        <TableHeaderColumn dataField='base_xp'>Base XP</TableHeaderColumn>
                        <TableHeaderColumn dataField='base_stat_total'>Stat Total</TableHeaderColumn>
                        <TableHeaderColumn dataField='ability_count'># of Abilities</TableHeaderColumn>
                        <TableHeaderColumn dataField='moves_count'># of Moves</TableHeaderColumn>
                    </BootstrapTable>
                    : <FontAwesomeIcon className="my-2" icon={faCircleNotch} size="2x" spin color="grey" />}
            </div>

            <h2>Moves</h2>

            <div className="col-6 move-table-data">
                {moveData["response"] ?
                    <BootstrapTable hover striped data={moveData["response"]} options={options}>
                        <TableHeaderColumn isKey dataFormat={move_name_link} dataField='name'>
                            Name
                        </TableHeaderColumn>
                        <TableHeaderColumn dataField='generation'>
                            Generation
                        </TableHeaderColumn>
                        <TableHeaderColumn dataField='type'>
                            Type
                        </TableHeaderColumn>
                        <TableHeaderColumn dataField='accuracy'>
                            Accuracy
                        </TableHeaderColumn>
                        <TableHeaderColumn dataField='pp'>
                            PP
                        </TableHeaderColumn>
                        <TableHeaderColumn dataField='power' >
                            Power
                        </TableHeaderColumn>
                    </BootstrapTable>
                    : <FontAwesomeIcon className="my-2" icon={faCircleNotch} size="2x" spin color="grey" />}
            </div>

            <h2>Types</h2>

            <div className="col-6 type-table-data">
                {typeData["response"] ?
                    <BootstrapTable hover striped data={typeData["response"]} options={options}>
                        <TableHeaderColumn isKey dataFormat={type_name_link} dataField='name'>
                            Name
                        </TableHeaderColumn>
                        <TableHeaderColumn dataField='generation'>
                            Generation
                        </TableHeaderColumn>
                        <TableHeaderColumn dataField='moves_count'>
                            # of Moves
                        </TableHeaderColumn>
                        <TableHeaderColumn dataField='pokemon_count'>
                            # of Pokemon
                        </TableHeaderColumn>
                        <TableHeaderColumn dataField='dub_damage_count'>
                            # of 2x Damage
                        </TableHeaderColumn>
                        <TableHeaderColumn dataField='no_damage_count'>
                            # of No Damage
                        </TableHeaderColumn>
                    </BootstrapTable>
                    : <FontAwesomeIcon className="my-2" icon={faCircleNotch} size="2x" spin color="grey" />}
            </div>

        </center>
    </PageTemplate >
}

function pokemon_name_link(Cell, Row) {
    var link = `/pokemons/${Row["id"]}`
    return <Link to={link}>{Cell}</Link>
}

function type_name_link(Cell, Row) {
    var link = `/types/${Row["id"]}`
    return <Link to={link}>{Cell}</Link>
}

function move_name_link(Cell, Row) {
    var link = `/moves/${Row["id"]}`
    return <Link to={link}>{Cell}</Link>
}

export default SearchResultsPage;