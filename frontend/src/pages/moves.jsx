import React, { useEffect, useState } from "react";
import PageTemplate from "../components/template";
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Link, useParams } from "react-router-dom";
import NavBar from "../components/navbar";
import PaginationMenu from "../components/PaginationMenu";
import FilterMenu from "../components/FilterMenu";
import SortMenu from "../components/SortMenu";
import LocalSearch from "../components/LocalSearch";
import { color_dict } from ".";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleNotch } from '@fortawesome/free-solid-svg-icons'
import multi_word_search_alert_box_update from "../components/multi_word_search";

function Moves() {
  const [data, setData] = useState({});
  const [page_num, setPageNum] = useState(1)
  const [num_per_page, setNumPerPage] = useState(10)
  const [sort, setSort] = useState("name")
  const [desc, setDesc] = useState("asc")
  const [search, setSearch] = useState("")
  var sortableFilters = ["name", "generation", "type", "accuracy", "pp", "power"]
  var sortableFiltersPretty = ["Name", "Generation", "Type", "Accuracy", "Power Points", "Power"]
  var url = new URL(`${process.env.REACT_APP_API_LINK}/api/get_move_page/`)
  var instance = new window.Mark(document.querySelector(".table-data"));

  var options = {
    noDataText: "No data matches criteria"
  };

  var filter_defaults = {
    "generation_filter": [0, 8],
    "accuracy_filter": [0, 100],
    "type_filter": [""]
  }

  const [filters, setFilters] = useState(filter_defaults)

  var filter_types = {
    "generation_filter": "range",
    "accuracy_filter": "range",
    "type_filter": "dropdown"
  }

  var filter_options = {
    "type_filter": ["bug", "dark", "dragon", "electric", "fairy", "fighting", "fire", "flying", "ghost", "grass", "ground", "ice", "normal", "poison", "psychic", "rock", "steel", "water"]
  }

  useEffect(() => {
    multi_word_search_alert_box_update("multiWordSearchAlert", search);

    setData({});

    var params = {
      page_num, num_per_page, sort, desc, search
    }

    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))
    Object.keys(filters).forEach(key => url.searchParams.append(key, filters[key]))

    fetch(url)
      .then(res => res.json())
      .then(
        (result) => {
          setData(result);
        }
      )
  }, [page_num, num_per_page, sort, desc, filters, search]);

  useEffect(() => {
    instance.mark(search, { "className": "highlight" })
  }, [data])

  return (
    <PageTemplate color={color_dict["bulb"]}>
      <center><NavBar></NavBar>
        <div id="multiWordSearchAlert" className="alert alert-primary d-none col-6"></div>

        <br></br>
        <div className="col-11 col-lg-6 table-data-container" >
          <div className="offset-1 my-2">
            <FilterMenu currentFilters={filters} setterFilters={setFilters} defaultFilters={filter_defaults}
              filterTypes={filter_types} filterOptions={filter_options}>
            </FilterMenu>
            <SortMenu linkTo="/movesList" setterSort={setSort} setterDesc={setDesc} currentDesc={desc}
              currentFilter={sort} sortableFilters={sortableFilters} sortableFiltersPretty={sortableFiltersPretty}></SortMenu>

            <LocalSearch setterSearch={setSearch}></LocalSearch>
          </div>
          <div className="table-data">
            {data["response"] ?
              <BootstrapTable hover striped data={data["response"]} options={options}>
                <TableHeaderColumn isKey dataFormat={name_link} dataField='name'>
                  Name
                </TableHeaderColumn>
                <TableHeaderColumn dataField='generation'>
                  Generation
                </TableHeaderColumn>
                <TableHeaderColumn dataField='type'>
                  Type
                </TableHeaderColumn>
                <TableHeaderColumn dataField='accuracy'>
                  Accuracy
                </TableHeaderColumn>
                <TableHeaderColumn dataField='pp'>
                  PP
                </TableHeaderColumn>
                <TableHeaderColumn dataField='power' >
                  Power
                </TableHeaderColumn>
              </BootstrapTable>
              : <FontAwesomeIcon className="my-2" icon={faCircleNotch} size="2x" spin color="grey" />}
          </div>
          <PaginationMenu setterNumPerPage={setNumPerPage} currentPerPage={num_per_page} setterPageNum={setPageNum}
            className="m-2 btn btn-primary" page_data={data["pages"]} linkTo="/movesList/"></PaginationMenu>

        </div>
      </center >
    </PageTemplate >
  );
};

function name_link(Cell, Row) {
  var link = `/moves/${Row["id"]}`
  return <Link to={link}>{Cell}</Link>
}
export default Moves;