import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import PageTemplate from "../components/template";
import NavBar from "../components/navbar";
import { color_dict } from ".";
import SingleDataContainer from "../components/single_data_cointainer";
import HideableDiv from "../components/hideable_div";
import get_full_list from "../components/get_full_list";

//Functional Component
function TypePage() {
    const [data, setData] = useState({});
    const { id } = useParams();
    const [isShowingPokemon, setIsShowingPokemon] = useState(false);
    const [isShowingMoves, setIsShowingMoves] = useState(false);
    const [isShowingDoubleDamage, setIsShowingDoubleDamage] = useState(false);
    const [isShowingNoDamage, setIsShowingNoDamage] = useState(false);
    const file_path = `/images/${id}.jpeg`

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_LINK}/api/get_specific_type/${id}`)
            .then(res => res.json())
            .then(
                (result) => {
                    setData(result["response"][0]);
                })
    }, [])


    var full_list_moves = get_full_list(data['move_list'], data['move_list_id'], "moves");
    var full_list_pokemon = get_full_list(data['pokemon_list'], data['pokemon_list_id'], "pokemons");
    var full_list_double_damage = get_full_list(data['double_damage_list'], data['double_damage_list_id'], "types");
    var full_list_no_damage = get_full_list(data['no_damage_list'], data['no_damage_list_id'], "types");

    return (
        <PageTemplate color={color_dict["squt"]}>
            <center><NavBar num={10} desc="asc" ></NavBar></center>

            <SingleDataContainer>
                <h1 className="displaypageTitle">{data['name']}</h1>
                <hr></hr>
                <img className="align-self-center col-8 img img-responsive" src={file_path} alt="Pokemon Type" /> < br />
                <span className="individual-data-header">Name: </span>{data['name']} <br />
                <span className="individual-data-header"># of Pokemon: </span>{data['pokemon_count']} < br />

                <HideableDiv setter={setIsShowingPokemon} trackingVariable={isShowingPokemon}
                    showMessage="Show Pokemon List" hideMessage="Hide Pokemon List">
                    {full_list_pokemon}
                </HideableDiv>
                <span className="individual-data-header"># of Moves: </span>{data['moves_count']} <br />

                <HideableDiv setter={setIsShowingMoves} trackingVariable={isShowingMoves}
                    showMessage="Show Moves List" hideMessage="Hide Moves List">
                    {full_list_moves}
                </HideableDiv>
                <span className="individual-data-header"># Double Damage to: </span>{data['double_damage_list'] ? data["double_damage_list"].length : "0"} <br />

                <HideableDiv setter={setIsShowingDoubleDamage} trackingVariable={isShowingDoubleDamage}
                    showMessage="Show 2x Damage Types" hideMessage="Hide 2x Damage Types">
                    {full_list_double_damage}
                </HideableDiv>

                <span className="individual-data-header"># No Damage to: </span>{data['no_damage_list'] ? data['no_damage_list'].length : "0"}<br />

                <HideableDiv setter={setIsShowingNoDamage} trackingVariable={isShowingNoDamage}
                    showMessage="Show No Damage Types" hideMessage="Hide No Damage Types">
                    {full_list_no_damage}
                </HideableDiv>

            </SingleDataContainer>

        </PageTemplate >
    );

};


export default TypePage;