import React from "react";

import JustifiedFlex from "../components/components";
import PageTemplate from "../components/template";

import bulb from '../assets/imgs/bulbasaur.png'
import charm from '../assets/imgs/charmander.png'
import pika from '../assets/imgs/pikachu.png'
import squt from '../assets/imgs/squirtle.png'
import SearchBar from "../components/SearchBar";
import snorlax from '../assets/imgs/snorlax.png';

export var color_dict = {
  "charm": "#FF8787",
  "bulb": "#87FF9F",
  "squt": "#87DAFF",
  "pika": "#FFEF87",
  "snor": "#07758C"
}


export function Section(props) {
  return (
    <a className="col-8 col-md-4 col-lg-2  m-3 m-md-5 m-lg-1 link_container" style={{ backgroundColor: props.color }} href={props.url} role="button">
      <div className="jumbotron sec">
        <div className="secContainer">
          <img src={props.img} className="splash-img offset-md-1" alt="Splash" />
          <h1 className="secText mt-4">{props.name}</h1>
          <p className="lead">
          </p>
        </div>
      </div>
    </a>
  )
}

//Functional Component 
const Root = () => {
  return (
    <PageTemplate>
      <JustifiedFlex>
        <Section name="Pokemon" url="/pokemonsList/" img={charm} color={color_dict["charm"]} />
        <Section name="Moves" url="/movesList/" img={bulb} color={color_dict["bulb"]} />
        <Section name="Types" url="/typesList/" img={squt} color={color_dict["squt"]} />
        <Section name="Group 2" url="/group2/" img={snorlax} color={color_dict["snor"]} />
        <Section name="About Us" url="/aboutus/" img={pika} color={color_dict["pika"]} />
      </JustifiedFlex>
      <br></br>
      <center><SearchBar className="mt-5"></SearchBar></center>
    </PageTemplate>
  );
};

export default Root;