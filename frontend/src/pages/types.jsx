import React, { useEffect, useState } from "react";
import PageTemplate from "../components/template";
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Link } from "react-router-dom";
import NavBar from "../components/navbar"
import PaginationMenu from "../components/PaginationMenu";
import FilterMenu from "../components/FilterMenu";
import SortMenu from "../components/SortMenu";
import LocalSearch from "../components/LocalSearch";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleNotch } from '@fortawesome/free-solid-svg-icons'
import { color_dict } from ".";
import multi_word_search_alert_box_update from "../components/multi_word_search";

//Functional Component 
function Types() {
    const [data, setData] = useState({});
    const [page_num, setPageNum] = useState(1)
    const [num_per_page, setNumPerPage] = useState(10)
    const [sort, setSort] = useState("name")
    const [desc, setDesc] = useState("asc")
    const [search, setSearch] = useState("")

    var sortableFilters = ["name", "generation", "moves_count", "pokemon_count", "dub_damage_count", "no_damage_count"]
    var sortableFiltersPretty = ["Name", "Generation", "Move Count", "Pokemon Count", "2X Damage Count", "No Damage Count"]
    var url = new URL(`${process.env.REACT_APP_API_LINK}/api/get_type_page/`)
    var instance = new window.Mark(document.querySelector(".table-data"));

    var options = {
        noDataText: "No data matches criteria"
    };

    var filter_defaults = {
        "generation_filter": [0, 8],
        "moves_count_filter": [0, 150],
        "pokemon_count_filter": [0, 200],
    }

    const [filters, setFilters] = useState(filter_defaults)

    var filter_types = {
        "generation_filter": "range",
        "moves_count_filter": "range",
        "pokemon_count_filter": "range"
    }

    var filter_options = {}

    useEffect(() => {
        multi_word_search_alert_box_update("multiWordSearchAlert", search);

        setData({});

        var params = {
            page_num, num_per_page, sort, desc, search
        }

        Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))
        Object.keys(filters).forEach(key => url.searchParams.append(key, filters[key]))

        fetch(url)
            .then(res => res.json())
            .then(
                (result) => {
                    setData(result);
                }
            )
    }, [page_num, num_per_page, sort, desc, filters, search]);

    useEffect(() => {
        instance.mark(search, { "className": "highlight" })
    }, [data])

    return (
        <PageTemplate color={color_dict["squt"]}>
            <center><NavBar></NavBar>
                <div id="multiWordSearchAlert" className="alert alert-primary d-none col-6"></div>


                <br></br>
                <div className="col-11 col-lg-6 table-data-container" >
                    <div className="offset-1 my-2">
                        <FilterMenu currentFilters={filters} setterFilters={setFilters} defaultFilters={filter_defaults}
                            filterTypes={filter_types} filterOptions={filter_options}>
                        </FilterMenu>
                        <SortMenu linkTo="/typesList" setterSort={setSort} setterDesc={setDesc} currentDesc={desc}
                            currentFilter={sort} sortableFilters={sortableFilters} sortableFiltersPretty={sortableFiltersPretty}></SortMenu>

                        <LocalSearch setterSearch={setSearch}></LocalSearch>
                    </div>
                    <div className="table-data">

                        {data["response"] ?
                            <BootstrapTable hover striped data={data["response"]} options={options}>
                                <TableHeaderColumn isKey dataFormat={name_link} dataField='name'>
                                    Name
                                </TableHeaderColumn>
                                <TableHeaderColumn dataField='generation'>
                                    Generation
                                </TableHeaderColumn>
                                <TableHeaderColumn dataField='moves_count'>
                                    # of Moves
                                </TableHeaderColumn>
                                <TableHeaderColumn dataField='pokemon_count'>
                                    # of Pokemon
                                </TableHeaderColumn>
                                <TableHeaderColumn dataField='dub_damage_count'>
                                    # of 2x Damage
                                </TableHeaderColumn>
                                <TableHeaderColumn dataField='no_damage_count'>
                                    # of No Damage
                                </TableHeaderColumn>
                            </BootstrapTable>
                            : <FontAwesomeIcon className="my-2" icon={faCircleNotch} size="2x" spin color="grey" />}
                    </div>

                    <PaginationMenu setterNumPerPage={setNumPerPage} currentPerPage={num_per_page} setterPageNum={setPageNum}
                        className="m-2 btn btn-primary" page_data={data["pages"]} linkTo="/typesList/"></PaginationMenu>

                </div>
            </center>
        </PageTemplate >
    );
};

function name_link(Cell, Row) {
    var link = `/types/${Row["id"]}`
    return <Link to={link}>{Cell}</Link>
}

export default Types;
