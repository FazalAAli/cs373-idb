import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import PageTemplate from "../components/template";
import NavBar from "../components/navbar";
import { color_dict } from ".";
import SingleDataContainer from "../components/single_data_cointainer";
import HideableDiv from "../components/hideable_div";
import get_full_list from "../components/get_full_list";

// Functional Component
function PokemonPage() {
    const [data, setData] = useState({});
    const { id } = useParams();
    const [isShowing, setIsShowing] = useState(false);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_LINK}/api/get_specific_pokemon/${id}`)
            .then(res => res.json())
            .then(
                (result) => {
                    setData(result["response"][0]);
                }
            )
    }, [])


    var full_list = get_full_list(data['move_list'], data['move_list_id'], "moves");
    return (
        <PageTemplate color={color_dict["charm"]}>
            <center><NavBar num={10} desc="asc"></NavBar></center>

            <SingleDataContainer>

                <h1 className="displaypageTitle">
                    {data["sprite"] ? <img src={data["sprite"]} alt="Pokemon Sprite" /> : null}
                    {data['name']}
                </h1>

                {data["card_image"] ? <img className="img img-responsive col-6 py-4" src={data["card_image"]} alt="Pokemon card" /> : null}
                <br></br>
                <span className="individual-data-header">Type: </span><Link to={`/types/${data['type_id']}`}>{data['type']}</Link> <br />
                <span className="individual-data-header">Weight: </span>{data['weight']} kg <br />
                <span className="individual-data-header">Height: </span>{data['height']} m<br />
                <span className="individual-data-header">Base XP: </span>{data['base_xp']} <br />
                <span className="individual-data-header">Base Stats: </span>{data['base_stat_total']} <br />
                <span className="individual-data-header"># of Abilities: </span>{data['ability_count']} <br />
                <span className="individual-data-header"># of Moves: </span>{data['moves_count']} <br />

                <HideableDiv setter={setIsShowing} trackingVariable={isShowing}
                    showMessage="Show More Moves" hideMessage="Show Less Moves">
                    {full_list}
                </HideableDiv>

            </SingleDataContainer>
        </PageTemplate >
    );
};

export default PokemonPage;