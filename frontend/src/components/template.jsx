import header_logo from '../assets/imgs/logo.png'
import Footer from './footer';
function PageTemplate(props) {
    return (
        <div className="main-splash-bg min-vh-100" style={{ backgroundColor: props.color }}>
            <center><a href="/"><img className="img img-responsive mt-4 col-12 col-md-3" alt="Pokemon Splash Navigation" src={header_logo}></img></a></center>
            <div className="splash_link_container mt-lg-5 pt-lg-5 container-flex">{props.children}</div>
            <Footer></Footer>
        </div>

    )
}

export default PageTemplate;