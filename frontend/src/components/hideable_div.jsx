function HideableDiv(props) {
    if (props.children.props.children[1].length > 0) {
        return (

            <div>
                <button className="my-3 btn btn-outline-primary" onClick={() => { props.setter(!props.trackingVariable) }}>
                    {!props.trackingVariable ? props.showMessage : props.hideMessage}
                </button>
                <br />
                {props.trackingVariable ? <div>{props.children}</div> : null}
            </div>)
    } else {
        return null;
    }
}


export default HideableDiv;