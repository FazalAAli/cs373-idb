function JustifiedFlex(props) {
	return (
		<div className="d-flex flex-wrap justify-content-around">
			{props.children}
		</div>
	)
}

export default JustifiedFlex;