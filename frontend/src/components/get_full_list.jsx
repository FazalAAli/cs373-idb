function get_full_list(name_list, name_list_ids, tag) {
    const list = []
    if (name_list && name_list_ids) {
        for (var i = 0; i < name_list.length; i++) {
            //list += move_list[i] + " ";

            list.push(<a className="px-1" href={`/${tag}/${name_list_ids[i]}`}>{name_list[i]}</a>);

            if ((i + 1) % 5 === 0) {
                list.push(<br></br>);
            }
        }
    }
    var div_id = `hidden_${tag}_list`
    return (<div id={div_id} > {list}</div >);
}

export default get_full_list;