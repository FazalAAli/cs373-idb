
function multi_word_search_alert_box_update(identifier, search) {
    var multi_word_search_alert = document.getElementById(identifier);
    if (search.includes("&&") || search.includes("and")) {
        multi_word_search_alert.innerHTML = "Multi Word Search Detected - Showing AND results <br> Use space or \"||\" to do an OR search"
        multi_word_search_alert.classList.remove("d-none");
        multi_word_search_alert.classList.add("d-block");
    } else if (search.includes(" ")) {
        multi_word_search_alert.innerHTML = "Multi Word Search Detected - Showing OR results <br> Use \"&&\" or \"and\" to do an AND search"
        multi_word_search_alert.classList.remove("d-none");
        multi_word_search_alert.classList.add("d-block");
    } else {
        multi_word_search_alert.classList.add("d-none");
        multi_word_search_alert.classList.remove("d-block");
    }
}

export default multi_word_search_alert_box_update;