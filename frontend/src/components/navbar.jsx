import React, { Component } from "react";
import { color_dict } from "../pages/index";
import bulb from '../assets/imgs/bulbasaur.png'
import charm from '../assets/imgs/charmander.png'
import pika from '../assets/imgs/pikachu.png'
import squt from '../assets/imgs/squirtle.png'
import snor from '../assets/imgs/snorlax.png'
import SideLink from "./side_link";
import SearchBar from "./SearchBar";

function NavBar(props) {
  return (
    <div className="container">
      <div className="row offset-lg-2 side-link-container">
        <SideLink name="Pokemons" url={`/pokemonsList/`} img={charm} color={color_dict["charm"]}></SideLink>
        <SideLink name="Moves" url={`/movesList/`} img={bulb} color={color_dict["bulb"]}></SideLink>
        <SideLink name="Types" url={`/typesList/`} img={squt} color={color_dict["squt"]}></SideLink>
        <SideLink name="Group 2" url={`/group2/`} img={snor} color={color_dict["snor"]}></SideLink>
        <SideLink name="About" url="/aboutus" img={pika} color={color_dict["pika"]}></SideLink>
      </div>

      <div className="row">
        <SearchBar className="mx-2 my-2"></SearchBar>
      </div>
    </div>
  );
}

export default NavBar;
