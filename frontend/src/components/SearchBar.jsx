import React, { Component } from "react";
import { Link } from "react-router-dom";

class SearchBar extends Component {
    render() {
        return (
            <div className={this.props.className}>
                <form action="/search/" method="get">
                    <input required name="query" id="searchParameter" className="input col-2 border-1 rounded-3" placeholder="" autocomplete="off"></input>
                    <input title="Search" type="submit" className="btn btn-primary mx-3" value="Search"></input>
                </form >
            </div >
        )
    }
}

export default SearchBar;