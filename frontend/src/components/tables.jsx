function TableTemplate(props) {
    return (
        <div className="backg_1 min-vh-100">
            {props.children}
        </div>
    )
}

export default TableTemplate;