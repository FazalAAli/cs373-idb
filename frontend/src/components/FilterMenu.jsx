

function FilterMenu(props) {
    var interactive_filters = get_filter_list(props);
    return (<span className="my-1">
        <span className="d-none" id="filters_menu">
            {interactive_filters}
            <br></br>
            <button className="btn btn-secondary m-3" onClick={() => apply_filters(props)}>Apply</button>
            <button className="btn btn-secondary m-3" onClick={() => clear_filters(props)}>Reset</button>
            <br></br>
        </span>
        <button className="btn btn-secondary my-3" onClick={() => showSpan("filters_menu")}>Filters</button>

    </span >
    )
}

function get_filter_list(props) {
    var list = []

    Object.entries(props.currentFilters).forEach(([key, value]) => {
        var filter_name = key.slice(0, -7);
        filter_name = filter_name.charAt(0).toUpperCase() + filter_name.slice(1);
        list.push(filter_name);
        list.push(<br></br>)
        list.push(show_filter_inputs(props, key))
        list.push(<br></br>)
    });

    return list;
}

function show_filter_inputs(props, key) {
    var type_of_filter = props.filterTypes[key];

    if (type_of_filter == "range") {
        return (
            <div>
                <label for="min">Min:</label>
                <input name="min" className="col-1 mx-2" ></input>
                <label for="max">Max:</label>
                <input name="max" className="col-1 mx-2" ></input>
            </div>)
    } else if (type_of_filter == "dropdown") {
        return (
            <select className="btn mt-2 btn-secondary dropdown-toggle" >
                <option></option>
                {props.filterOptions[key].map((value) => <option>{value}</option>)}
            </select >)
    }
}

function apply_filters(props) {
    var filter_holder = document.getElementById("filters_menu");
    var inputs = filter_holder.querySelectorAll("input, select");
    var inputs_iterator = inputs[Symbol.iterator]();
    var new_filters = {}
    var filter_keys = Object.keys(props.currentFilters);
    for (var key_index in filter_keys) {
        var values = props.currentFilters[filter_keys[key_index]]
        for (var i = 0; i < values.length; i++) {
            var possible_value = inputs_iterator.next().value.value;
            if (possible_value != "") {
                values[i] = possible_value;
            }
        }
        new_filters[filter_keys[key_index]] = values;
    }
    props.setterFilters(new_filters)
    showSpan("filters_menu")
}

function clear_filters(props) {
    var filter_holder = document.getElementById("filters_menu");
    var inputs = filter_holder.querySelectorAll("input, select");
    for (var i = 0; i < inputs.length; i++) {
        inputs[i].value = ""
    }

    props.setterFilters(props.defaultFilters);
    showSpan("filters_menu")
}

function showSpan(identifier) {
    var test_div = document.getElementById(identifier);
    test_div.classList.toggle("d-none");
    test_div.classList.toggle("d-inline");

}

export default FilterMenu;