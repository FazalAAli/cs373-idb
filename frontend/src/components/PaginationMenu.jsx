var page_num_values = ["5", "10", "20", "50", "100"]

function PaginationMenu(props) {
    if (props.page_data) {
        var page_nums = []
        if (props.page_data.length > 1) {
            for (var i = 0; i < (props.page_data).length; i++) {
                // Iterate over numeric indexes from 0 to 5, as everyone expects.
                if (props.page_data[i]) {
                    var num_to_display;
                    if (i == 0) {
                        num_to_display = props.page_data[i] + " (First)";
                    } else if (i == props.page_data.length - 1) {
                        num_to_display = props.page_data[i] + " (Last)";
                    } else {
                        num_to_display = props.page_data[i];
                    }
                    page_nums.push(<button value={props.page_data[i]} className={props.className} onClick={e => redirectPageNum(e.target.value, props)}> {num_to_display}</button>)
                } else {
                    page_nums.push(<h2 className={props.className} style={{ backgroundColor: "white", color: "black", cursor: "default" }} >...</h2>)
                }
            }
        } else {
            page_nums.push(<h2 className={props.className} style={{ backgroundColor: "white", color: "black", cursor: "default" }} >All entries shown</h2>)
        }

        return (
            <div className={props.containerClassName}>{page_nums}
                <label for="num_per_page" className={props.className} style={{ backgroundColor: "white", color: "black", cursor: "default" }}>Per page:</label>
                <select name="num_per_page" id="num_per_page" className="btn btn-secondary dropdown-toggle my-lg-0 my-2" onChange={() => redirectNumPerPage(props)} defaultValue={props.currentPerPage}>
                    {page_num_values.map((number) => <option>{number}</option>)}
                </select>
            </div>)
    } else {
        return <div></div>
    }
}

export default PaginationMenu;

function redirectPageNum(pageNum, props) {
    props.setterPageNum(pageNum)
}

function redirectNumPerPage(props) {
    var selector = document.getElementById("num_per_page")
    if (selector.value) {
        props.setterPageNum(1)
        props.setterNumPerPage(selector.value)
    }
}