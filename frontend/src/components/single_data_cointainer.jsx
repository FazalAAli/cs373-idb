function SingleDataContainer(props) {
    return (
        <center >
            <div className="container">
                <div className="individual-data-container col-6 mt-5">
                    {props.children}
                    <br />
                </div>
            </div>
        </center>
    );
}

export default SingleDataContainer;