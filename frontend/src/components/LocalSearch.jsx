
function LocalSearch(props) {
    return (
        <span>
            <form class="d-inline">
                <input name="query" id="localSearch" className="input mx-2 border-1 rounded-3" placeholder="Local Search" autocomplete="off"></input>
                <input onClick={(e) => set_search_param(e, props)} title="Search" type="submit" className="btn btn-secondary mx-1" value="Search">
                </input>
            </form>
        </span >)
}

function set_search_param(e, props) {
    e.preventDefault();
    var keyword = document.getElementById("localSearch").value;
    props.setterSearch(keyword);
}

export default LocalSearch;