function SideLink(props) {
    return (
        <a className="col-lg-2 col-sm-4 link mt-3" href={props.url}>
            <div className="rounded link_container" style={{ backgroundColor: props.color }}>
                <center>
                    <img className="img col-1 side-link-image" src={props.img} alt="Pokemon navigation" />
                    <p className="m-0">{props.name}</p>
                </center>
            </div >
        </a>
    )
}

export default SideLink;