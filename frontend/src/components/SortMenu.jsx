function SortMenu(props) {
    var current_filter_pretty = props.sortableFiltersPretty[props.sortableFilters.indexOf(props.currentFilter)];
    return (
        <span className={"my-1"}>
            <label for="page_filter" className="mx-2 my-lg-0 my-1" style={{ backgroundColor: "white", color: "black", cursor: "default" }}>Sort By:</label>
            <select name="page_filter" id="page_filter" className="btn my-lg-0 my-1 btn-secondary dropdown-toggle" onChange={() => redirectPageFilter(props)} defaultValue={current_filter_pretty}>
                {props.sortableFiltersPretty.map((value) => <option>{value}</option>)}
            </select>
            <button title="Change sort order" className="mx-2 my-lg-0 my-1 btn btn-secondary" id="directionChanger" onClick={() => changeDirections(props)}>{props.currentDesc == "desc" ? "Descending" : "Ascending"}</button>
        </span>)
}

function redirectPageFilter(props) {
    var selector = document.getElementById("page_filter")
    if (selector.value) {
        props.setterSort(props.sortableFilters[props.sortableFiltersPretty.indexOf(selector.value)])
    }
}

function changeDirections(props) {
    var nextMode = props.currentDesc == "desc" ? "asc" : "desc"
    props.setterDesc(nextMode)
}
export default SortMenu;
