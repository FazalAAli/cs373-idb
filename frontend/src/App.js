// import logo from './logo.svg';
import React from 'react';
//import NavBar from "./components/navbar";
//import Footer from "./components/footer";
import './App.css';
import {
    BrowserRouter as Router,
    Route,
    Switch
} from "react-router-dom";
import Root from "./pages";
import AboutUs from "./pages/aboutus";
import Pokemons from "./pages/pokemons";
import PokemonPage from "./pages/pokemon";
import Moves from "./pages/moves";
import MovePage from "./pages/move";
import Types from "./pages/types";
import TypePage from "./pages/type";
import SearchResultsPage from './pages/SearchResultsPage';
import Group2Page from './pages/group2';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';

function App() {
    return (
        <div className="background min-vh-100">
            <Router>
                <div>
                    <Switch>
                        <Route exact path="/">
                            <Root />
                        </Route>
                        <Route exact path="/aboutus">
                            <AboutUs />
                        </Route>
                        <Route exact path="/pokemonsList/">
                            <Pokemons />
                        </Route>
                        <Route exact path="/pokemons/:id">
                            <PokemonPage />
                        </Route>
                        <Route exact path="/movesList/">
                            <Moves />
                        </Route>
                        <Route exact path="/moves/:id">
                            <MovePage />
                        </Route>
                        <Route exact path="/typesList/">
                            <Types />
                        </Route>
                        <Route exact path="/types/:id">
                            <TypePage />
                        </Route>
                        <Route exact path="/search/">
                            <SearchResultsPage />
                        </Route>
                        <Route exact path="/group2/">
                            <Group2Page />
                        </Route>
                    </Switch>
                </div>
            </Router>
        </div>
    );
}


export default App;
