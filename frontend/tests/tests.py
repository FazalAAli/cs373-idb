import unittest
import os
import requests
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait

# AUTOMATED TEST COUNT: JUST ADD two at signs and your name before each test you write so it can be counted
# Be sure to use lowercase name and have it be one of the following
# fazal, carter, lawson, rice, trevor
# Please see test_title for an example of automated couting


def select_driver(os_name, browser, version):
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--log-level=3")
    chrome_options.add_experimental_option(
        'excludeSwitches', ['enable-logging'])

    current_path = os.path.dirname(os.path.realpath(__file__))
    if os_name == 'Linux' and browser == 'Chrome' and version == 91:
        return webdriver.Chrome(os.path.abspath(os.path.join(current_path, "chromedriver")), options=chrome_options)
    elif os_name == 'Windows' and browser == 'Chrome' and version == 91:
        return webdriver.Chrome(os.path.abspath(os.path.join(current_path, "chromedriver.exe")), options=chrome_options)  # nopep8
    else:
        raise ValueError('Driver not supported.')


'''
Should ideally read the operating system and determine
which driver to use.
'''


def get_driver():
    # we completely forget MacOS exists
    os_name = 'Windows' if os.name == "nt" else 'Linux'
    browser = 'Chrome'
    version = 91
    return select_driver(os_name, browser, version)


def select_baseurl():
    url = "https://cs373-pokedex.com"
    try:
        x = requests.get("http://127.0.0.1:3000")
        url = "http://127.0.0.1:3000"
    except:
        pass

    return url


def get_baseurl():
    return select_baseurl()


class FrontEndTests(unittest.TestCase):
    driver = get_driver()
    baseurl = get_baseurl()

    def setUp(self):
        self.url = lambda x: self.baseurl + x

    # @@trevor
    def test_title(self):
        driver = self.driver
        driver.get(self.url('/'))
        assert "CS373" in driver.title

    # @@trevor
    def test_title_about(self):
        driver = self.driver
        driver.get(self.url('/aboutus'))
        assert "CS373" in driver.title

    # @@carter
    def test_title_pokemons_pillar(self):
        driver = self.driver
        driver.get(self.url('/api/get_pokemon_page/'))
        assert "CS373" in driver.title

    # @@carter
    def test_title_types_pillar(self):
        driver = self.driver
        driver.get(self.url('/api/get_type_page/'))
        assert "CS373" in driver.title

    # @@carter
    def test_title_moves_pillar(self):
        driver = self.driver
        driver.get(self.url('/api/get_moves_page/'))
        assert "CS373" in driver.title

    # @@trevor
    def test_pokemon_search(self):
        driver = self.driver
        driver.get(self.url('/pokemonsList'))
        assert "Search" in driver.page_source
        assert "Filters" in driver.page_source
        assert "Name" in driver.page_source

    # @@trevor
    def test_pokemon_content_loaded(self):
        driver = self.driver
        driver.get(self.url('/pokemonsList'))
        WebDriverWait(driver, timeout=4).until(
            lambda d: d.find_element_by_tag_name("td"))
        assert "grass" in driver.page_source

    # @@trevor
    def test_pokemon_content_loaded_psychic(self):
        driver = self.driver
        driver.get(self.url('/pokemonsList'))
        WebDriverWait(driver, timeout=4).until(
            lambda d: d.find_element_by_tag_name("td"))
        assert "psychic" in driver.page_source

    # @@trevor
    def test_pokemon_content_loaded_dark(self):
        driver = self.driver
        driver.get(self.url('/pokemonsList'))
        WebDriverWait(driver, timeout=4).until(
            lambda d: d.find_element_by_tag_name("td"))
        assert "dark" in driver.page_source

    # @@trevor
    def test_pokemon_content_loaded_bug(self):
        driver = self.driver
        driver.get(self.url('/pokemonsList'))
        WebDriverWait(driver, timeout=4).until(
            lambda d: d.find_element_by_tag_name("td"))
        assert "bug" in driver.page_source

    # @@trevor
    def test_moves_content_loaded_electric(self):
        driver = self.driver
        driver.get(self.url('/movesList'))
        WebDriverWait(driver, timeout=4).until(
            lambda d: d.find_element_by_tag_name("td"))
        assert "electric" in driver.page_source

    # @@trevor
    def test_moves_content_loaded_grass(self):
        driver = self.driver
        driver.get(self.url('/movesList'))
        WebDriverWait(driver, timeout=4).until(
            lambda d: d.find_element_by_tag_name("td"))
        assert "grass" in driver.page_source

    # @@trevor
    def test_moves_content_loaded_name_acid(self):
        driver = self.driver
        driver.get(self.url('/movesList'))
        WebDriverWait(driver, timeout=4).until(
            lambda d: d.find_element_by_tag_name("td"))
        assert "acid" in driver.page_source

    # @@lawson
    def test_pokemon_content(self):
        driver = self.driver
        driver.get(self.url('/pokemonsList'))

        # Checking Nav Bar
        assert "Pokemons" in driver.page_source
        assert "Moves" in driver.page_source
        assert "Types" in driver.page_source
        assert "About" in driver.page_source

    # @@lawson
    def test_moves_content(self):
        driver = self.driver
        driver.get(self.url('/movesList'))

        # Checking Nav Bar
        assert "Pokemons" in driver.page_source
        assert "Moves" in driver.page_source
        assert "Types" in driver.page_source
        assert "About" in driver.page_source

    # @@lawson
    def test_types_content(self):
        driver = self.driver
        driver.get(self.url('/typesList'))

        # Checking Nav Bar
        assert "Pokemons" in driver.page_source
        assert "Moves" in driver.page_source
        assert "Types" in driver.page_source
        assert "About" in driver.page_source


if __name__ == "__main__":
    unittest.main()
